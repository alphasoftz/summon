<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title><?php echo ($title=='')?WEBSITE_NAME:$title; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />  
	<meta name="description" content="<?php echo ($meta_desc=='')?META_DESC:$meta_desc; ?>" />
	<meta name="keywords" content="<?php echo ($meta_keywords=='')?META_KEYWORDS:$meta_keywords; ?>" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>style/style.css" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>style/prettyPhoto.css" />
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,700,300,900' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Nunito:400,300,700' rel='stylesheet' type='text/css' />
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>style/stylemobile.css" />
	<!-- <link rel="stylesheet" type="text/css" media="all" href="style/mobilenavigation.css" /> -->
	<script src="<?php echo base_url(); ?>script/modernizr.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>script/jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>script/jquery-ui.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>script/jquery.flexslider.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>script/jquery.prettyPhoto.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>script/jquery.retina.js" type="text/javascript"></script>
	<style type="text/css">
	#slider-news .flexslider .slides img { 
	  display: block;
	  max-width: 100%; height: 530px; margin: 0 auto;
	}
	

	  #homepage_slider .slides img {
	    width: 100%;
	  }
	  #slider-news {
  margin: 0 0 10px 0;
  position: relative;
}
  </style>
  
	<script type="text/javascript">
	$(document).ready(function (){
        $(window).scroll(function () {
            if ($(document).scrollTop() <= 40) {
                $('#header-full').removeClass('small');
                $('.tabs-blur').removeClass('no-blur');
                $('#main-header').removeClass('small');
            } else {
                $('#header-full').addClass('small');
                $('.tabs-blur').addClass('no-blur');
                $('#main-header').addClass('small');
            }
        });
        
        $("a[data-rel^='prettyPhoto']").prettyPhoto({
			default_width: 600,
			default_height: 420,
			social_tools: false
		});

        $('#slideshow-tabs').tabs({ show: { effect: "fade", duration: 200 }, hide: { effect: "fade", duration: 300 } });
        $('.slider-tabs.flexslider').flexslider({
            animation: "slide",
            pauseOnAction: true
        });

		$('a[data-rel]').each(function() {
			$(this).attr('rel', $(this).data('rel'));
		});
		$('img[data-retina]').retina({checkIfImageExists: true});
		$(".open-menu").click(function(){
		    $("body").addClass("no-move");
		});
		$(".close-menu, .close-menu-big").click(function(){
		    $("body").removeClass("no-move");
		});
		
        $('.slider-partners.flexslider').flexslider({
            animation: "slide",
            pauseOnAction: true,
            itemWidth: 163,
            itemMargin: 0
        });
        $('#slider-news.flexslider').flexslider({
            animation: "slide",
            pauseOnAction: true
        });
	});
	</script>
</head>
<body>
	<?php include('site_header.php'); ?>
    <?php //include('slider.php');?>
    <div id="content-container">
        <div id="content" class="clearfix">
            
            <div id="main-content">
                <!--<div id="breadcrumbs" class="clearfix">
                    <div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="index-2.html" itemprop="url" class="icon-home">
                            <span itemprop="title">Home</span>
                        </a> <span class="arrow">&gt;</span>
                    </div>  
                    <div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="#" itemprop="url">
                            <span itemprop="title">About Us</span>
                        </a> <span class="arrow">&gt;</span>
                    </div>  
                    <div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="#" itemprop="url">
                            <span itemprop="title">History</span>
                        </a> <span class="arrow">&gt;</span>
                    </div>
                    <span class="last-breadcrumbs">
                        Greeting from our Principal - Mrs. John Doe
                    </span>
                </div>-->
                <article class="static-page">
                	<?php
                	foreach ($page_data as $key => $value) 
                	{
                		$v_pageTitle=stripcslashes($value->page_title);
                		$v_pageContent=stripcslashes($value->page_content);
                	}
                	?>
                    <h1 id="main-title"><?php echo $v_pageTitle; ?></h1>
                    <?php
                    if($value->page_images!='')
                    {
                    	foreach ($imagearray as $key => $value) 
		                {
		                	$parsed[]=$value;
		                }
                    	?>
                    	<div id="slider-news" class="flexslider">
		                    <ul class="slides">
		                    	<?php
		                    	//print_r($parsed);
		                    	foreach ($parsed as $key => $value) 
		                    	{
		                    	?>
		                        <li>
		                            <div class="slider-news-content">
						                <img src="<?php echo base_url().$value[0]->gallery_image; ?>" alt="News Title" />
		                                <div class="panel-slider-news">
		                                    <!--<ul class="category-slider clearfix">
		                                        <li><a href="#">Science &amp; Health</a></li>
		                                        <li><a href="#">Student Achievement</a></li>
		                                    </ul>-->
		                                    <h2><a href="#"><?php echo $value[0]->gallery_title;?></a></h2>
		                                </div>
		                            </div>
		                        </li>
		                        <?php
		                   		}
		                   		?>
							</ul>
		                </div>
                     <?php
                	}
                	?>	
                    <!--<img src="images/img-12.png" alt="" class="alignleft">-->
                    <?php echo $v_pageContent; ?>
                   
                </article>
            </div>
            <div id="sidebar" class="sidebar-homepage">
            	<!-- DISPLAY SIDEBAR MENUS-->
	            <?php
	            foreach ($menuarray as $key => $value) 
	            {
	            	$parsedmenu[]=$value;
	            }
	    			foreach ($parsedmenu as $key => $value) 
	                {
	            	?>
	            	<aside class="widget-container">
	                    <div class="widget-wrapper clearfix">
					        <h3 class="widget-title"><?php echo $value[0]->menu_name; ?></h3>
	                        <ul>
	                        	<?php
	                        	foreach ($this->en_menu_model->getMenuItemsForMenu($value[0]->menuid) as $key => $value) {
	                        		# code...
	                        		?>
	                        		<li><a href="<?php echo base_url().str_replace(" ", "-", $value->display_name); ?>"><?php echo $value->display_name; ?></a></li>
	                        		<?php
	                        	}
	                        	?>
	                            
	                        </ul>
	                    </div>
	                </aside>
	            	<?php
	            }
	            ?>


                <!-- DISPLAY NEWS-->
                <?php
				if(isset($sbitval) && in_array('2', $sbitval))
				{
				?>
                <aside class="widget-container">
				        <div class="widget-wrapper clearfix">
				            <h3 class="widget-title">Latest News</h3>
				                <ul class="menu news-sidebar">	
				                	<?php
			                        foreach ($sb_news_data as $key => $value) 
			                        {
									?>		
								    <li class="clearfix">
                                        <h4><a href="news/<?php echo str_replace(" ", "-", $value->news_title); ?>"><?php echo $value->news_title; ?></a></h4>
                                        <p><?php echo substr($value->news_desc,0,100); ?></p>
								        <span class="date-news"><?php echo date('F d,Y', strtotime($value->created_date));?></span>
								    </li>
								    <?php
									}
									?>
								</ul>
								<?php
								//if(count($sb_news_data)>5)
								{
								?>
								<a href="<?php echo base_url().'News';?>" class="button-more">Read News</a>
								<?php
				            	}
				            	?>
				        </div>
				</aside>
				<?php
           		} 
                ?>

				<!-- DISPLAY EVENTS-->
				<?php
				if(isset($sbitval) && in_array('3', $sbitval))
				{
				?>
                <aside class="widget-container">
                    <div class="widget-wrapper clearfix">
				        <h3 class="widget-title">Latest Events</h3>
                        <ul class="menu event-sidebar">		
	                        <?php
	                        foreach ($sb_events_data as $key => $value) 
	                        {
							?>
							    <li class="clearfix">
							        <div class="event-date-widget">
	                                    <strong><?php echo date('d', strtotime($value->event_start_date));?></strong>
	                                    <span><?php echo date('M', strtotime($value->event_start_date));?></span>
	                                </div>
	                                <div class="event-content-widget">
	                                    <article>
	                                        <h4><a href="#"><?php echo $value->events_title; ?></a></h4>
	                                        <p><?php echo date('F d,Y', strtotime($value->event_start_date));?> <?php echo ($value->event_end_date=='0000:00:00 00:00:00')?'':'-'.date('F d,Y', strtotime($value->event_end_date)); ?><br>
	                                            <?php echo date('h:i a', strtotime($value->event_start_date));?> <?php echo ($value->event_end_date=='0000:00:00 00:00:00')?'':'-'.date('h:i a', strtotime($value->event_end_date)); ?>
	                                        </p>
	                                        <em><?php echo $value->events_location; ?></em>
	                                    </article>
	                                </div>
							    </li>
	                        <?php
	                        }
	                        ?>    
						</ul>
						<?php
					//	if(count($sb_news_data)>5)
						{
						?>
                        	<a href="<?php echo base_url().'Events';?>" class="button-more">More Events</a>
                        <?php
                    	}
                    	?>
                    </div>
                </aside>
               <?php
           		}
               ?>
               <?php
               if(isset($sbitval) && in_array('4', $sbitval))
               {
               	?>
               	<aside id="gw_gallery-5" class="widget-container widget_gw_gallery">
                    <div class="widget-wrapper clearfix">
                        <h3 class="widget-title">Photo Gallery</h3>  
                        <script type="text/javascript">
    				        jQuery(document).ready(function($){
    				            $("#gw_gallery-5-slide").flexslider({
    								animation: "slide",
    								animationLoop: false,
    								pauseOnAction: true
    				            });
    				        });
    				    </script>
                        <div id="gw_gallery-5-slide" class="flexslider">
                            <ul class="slides">
                            	<?php
		                        foreach ($sb_gallery_data as $key => $value) 
		                        {
								?>
    				            <li>
    				                <div class="slides-image">
                                    <a href="<?php echo $value->gallery_image; ?>" data-rel="prettyPhoto[pp-gw_gallery-5]"><img src="<?php echo $value->gallery_image; ?>" alt="<?php echo $value->gallery_title; ?>"  data-retina="<?php echo $value->gallery_image; ?>" /></a>
                                    </div>
                                    <h4><a href="<?php echo $value->gallery_image; ?>" data-rel="prettyPhoto[pp-gw_gallery-5-slide]"><?php echo $value->gallery_title; ?></a></h4>
    				            </li>
    				            <?php
    				        	}
    				        	?>
    				        </ul>
                        </div>
                    </div>
                </aside>
               	<?php
               }
               ?>
            </div>
        </div>
    </div>
    <?php include('site_footer.php'); ?>
</body>
</html>