<!DOCTYPE html>
    <!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo ( isset( $title ) ) ? $title : APPLICATION_NAME; ?></title>
        <link rel="shortcut icon" href="<?php echo base_url(); ?>img/icons/favicon.ico" />
        <!-- Vendor CSS -->
        <link href="<?php echo base_url(); ?>vendors/animate-css/animate.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendors/sweet-alert/sweet-alert.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendors/material-icons/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendors/socicon/socicon.min.css" rel="stylesheet">
            
        <!-- CSS -->
        <link href="<?php echo base_url(); ?>css/app.min.1.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/app.min.2.css" rel="stylesheet">
        <style type="text/css">
        .row
        {
            margin-right: 0px !important; 
        }
        </style>
    </head>
    
    <body class="login-content">

        <div class="row">
            <div class="col-md-12" style="margin-bottom:100px;">
                <h1 style="color:#fff"><?php echo WEBSITE_NAME; ?></h1>
                <h3 id="clock" style="color:#fff">Time</h3>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <div class="lc-block toggled" style="width:90%" id="l-login">
                    <h2>Traffic in Malyasia</h2>
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                      <!-- Indicators -->
                      <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                      </ol>

                      <!-- Wrapper for slides -->
                      <div class="carousel-inner" role="listbox">
                        <div class="item active">
                          <img src="<?php echo base_url();?>/img/traffic1.jpg" alt="...">
                          <div class="carousel-caption">
                            ...
                          </div>
                        </div>
                        <div class="item">
                          <img src="<?php echo base_url();?>/img/traffic2.jpg" alt="...">
                          <div class="carousel-caption">
                            ...
                          </div>
                        </div>
                        <div class="item">
                          <img src="<?php echo base_url();?>/img/traffic3.jpg" alt="...">
                          <div class="carousel-caption">
                            ...
                          </div>
                        </div>
                      </div>

                      <!-- Controls -->
                      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
                
                </div>
            </div>
            <div class="col-md-6">
                <?php echo ( isset( $content ) ) ? $content : ''; ?>
                <br><br><h4>Secured payments through </h4>
                
                <img src="<?php echo base_url(); ?>img/logo_paypal.png" width="275" height="95"><br>
                <h5>Are you a smartphone user? <a href="">Download android application!</a></h5>
            </div>
        </div>
        
        
        
       
        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1 class="c-white">IE SUCKS!</h1>
                <p>You are using an outdated version of Internet Explorer, upgrade to any of the following web browser <br/>in order to access the maximum functionality of this website. </p>
                <ul class="iew-download">
                    <li>
                        <a href="http://www.google.com/chrome/">
                            <img src="img/browsers/chrome.png" alt="">
                            <div>Chrome</div>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.mozilla.org/en-US/firefox/new/">
                            <img src="img/browsers/firefox.png" alt="">
                            <div>Firefox</div>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.opera.com">
                            <img src="img/browsers/opera.png" alt="">
                            <div>Opera</div>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.apple.com/safari/">
                            <img src="img/browsers/safari.png" alt="">
                            <div>Safari</div>
                        </a>
                    </li>
                    <li>
                        <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                            <img src="img/browsers/ie.png" alt="">
                            <div>IE (New)</div>
                        </a>
                    </li>
                </ul>
                <p>Upgrade your browser for a Safer and Faster web experience. <br/>Thank you for your patience...</p>
            </div>   
        <![endif]-->
        
        <!-- Javascript Libraries -->
        <script src="<?php echo base_url(); ?>js/jquery-2.1.1.min.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
        
        <script src="<?php echo base_url(); ?>vendors/waves/waves.min.js"></script>
        
        <script src="<?php echo base_url(); ?>js/functions.js"></script>
        <script type="text/javascript">
            function updateClock ( )
            {
            var currentTime = new Date ( );
            var currentDate = currentTime.getDate ( );
            var currentMonth = currentTime.getMonth ( );
            var currentYear = currentTime.getFullYear ( );
            var currentHours = currentTime.getHours ( );
            var currentMinutes = currentTime.getMinutes ( );
            var currentSeconds = currentTime.getSeconds ( );

            // Pad the minutes and seconds with leading zeros, if required
            currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
            currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

            // Choose either "AM" or "PM" as appropriate
            var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";

            // Convert the hours component to 12-hour format if needed
            currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

            // Convert an hours component of "0" to "12"
            currentHours = ( currentHours == 0 ) ? 12 : currentHours;

            // Compose the string for display
            var currentTimeString = currentDate+"/"+currentMonth+"/"+currentYear+" "+currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;
            
            
            $("#clock").html(currentTimeString);
                
         }

        $(document).ready(function()
        {
           setInterval('updateClock()', 1000);
        });
        </script> 
    </body>
</html>