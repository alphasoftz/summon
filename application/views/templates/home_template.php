<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />  
	<meta name="description" content="Website Description" />
	<meta name="keywords" content="Website Keywords" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>style/style.css" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>style/prettyPhoto.css" />
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,700,300,900' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Nunito:400,300,700' rel='stylesheet' type='text/css' />
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>style/stylemobile.css" />
	<!-- <link rel="stylesheet" type="text/css" media="all" href="style/mobilenavigation.css" /> -->
	<script src="<?php echo base_url(); ?>script/modernizr.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>script/jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>script/jquery-ui.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>script/jquery.flexslider.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>script/jquery.prettyPhoto.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>script/jquery.retina.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(document).ready(function (){
        $(window).scroll(function () {
            if ($(document).scrollTop() <= 40) {
                $('#header-full').removeClass('small');
                $('.tabs-blur').removeClass('no-blur');
                $('#main-header').removeClass('small');
            } else {
                $('#header-full').addClass('small');
                $('.tabs-blur').addClass('no-blur');
                $('#main-header').addClass('small');
            }
        });
        
        $("a[data-rel^='prettyPhoto']").prettyPhoto({
			default_width: 600,
			default_height: 420,
			social_tools: false
		});
        $('#slideshow-tabs').tabs({ show: { effect: "fade", duration: 200 }, hide: { effect: "fade", duration: 300 } });
        $('.slider-tabs.flexslider').flexslider({
            animation: "slide",
            pauseOnAction: true,
        });
		$('a[data-rel]').each(function() {
			$(this).attr('rel', $(this).data('rel'));
		});
		$('img[data-retina]').retina({checkIfImageExists: true});
		$(".open-menu").click(function(){
		    $("body").addClass("no-move");
		});
		$(".close-menu, .close-menu-big").click(function(){
		    $("body").removeClass("no-move");
		});
	});
	</script>
</head>
<body class="home">
	<?php include('site_header.php'); ?>
    <?php include('slider.php');?>
    <div id="content-container">
        <div id="content" class="clearfix">
         <!--  
          <div id="banner-homepage">
                <a href="#"><img src="images/img-2.jpg" alt="" /></a>
            </div>-->
            <div id="main-content">
            <?php echo (isset($content)? $content:''); ?>
            </div>
            <?php include('sidebar_home.php'); ?>
            <?php echo (isset($content_add)? $content_add:''); ?>
        </div>
    </div>
    <?php include('site_footer.php'); ?>
</body>
</html>