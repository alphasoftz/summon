<?php
$seg_ment_1=$this->uri->segment(1);
$seg_ment_2=$this->uri->segment(2);
$seg_ment_3=$this->uri->segment(3);
?>
<aside id="sidebar">
    <div class="sidebar-inner">
        <div class="si-inner">
            <div class="profile-menu">
                <a href="#">
                    <div class="profile-pic">
                        <img src="<?PHP echo base_url(); ?>img/user_icon.png" alt="">
                    </div>
                    
                    <div class="profile-info">
                        <?php $user = $this->ion_auth->user()->row();
                        echo ucfirst($user->first_name),' ',ucfirst($user->last_name); ?>
                        
                        <i class="md md-arrow-drop-down"></i>
                    </div>
                </a>
                
                <ul class="main-menu">
                    <li>
                        <a href="<?php echo base_url(); ?>auth/logout"><i class="md md-history"></i> Logout</a>
                    </li>
                </ul>
            </div>
            
            <?php
            if ($this->ion_auth->logged_in())
            {
                if($this->ion_auth->is_admin())
                {
            ?>
                    <ul class="main-menu">
                        <li><a href="<?php echo base_url(); ?>auth/index"><i class="md md-home"></i> Dashboard</a></li>
                        <li class="sub-menu <?php echo ($this->uri->segment(1)=='users' || $this->uri->segment(1)=='auth')?'active toggled':''; ?>">
                            <a href="#"><i class="md c-red md-account-child"></i> User Administration</a>
                            <ul>
                                <li><a <?php echo ($seg_ment_1=='users')?'class="active"':''; ?> href="<?php echo base_url(); ?>users">Users Management</a></li>
                                <li><a <?php echo ($seg_ment_1=='users' && $this->uri->segment(2)=='create')?'class="active"':''; ?> href="<?php echo base_url(); ?>users/create">Create New user</a></li>
                                <li><a <?php echo ($seg_ment_1=='users' && $this->uri->segment(2)=='group')?'class="active"':''; ?>href="<?php echo base_url(); ?>users/group/new">Create New Group</a></li>
                            </ul>
                        </li>
                        <li class="sub-menu <?php echo ($seg_ment_1=='master')?'active toggled':''; ?>">
                            <a href="#"><i class="md c-red md-settings"></i> Master Settings</a>
                            <ul>
                                <li><a <?php echo ($seg_ment_1=='master')?'class="active"':''; ?> href="<?php echo base_url(); ?>master/types">Summon Types</a></li>
                                
                            </ul>
                        </li>
                        <li class="sub-menu <?php echo ($seg_ment_1=='summon')?'active toggled':''; ?>">
                            <a href="#"><i class="md c-red md-account-balance"></i> Summons</a>
                            <ul>
                                <li><a <?php echo ($seg_ment_1=='summon' && $seg_ment_2=='create')?'class="active"':''; ?> href="<?php echo base_url(); ?>summon/create">New Summon</a></li>
                                <li><a <?php echo ($seg_ment_1=='summon' && $seg_ment_2=='manage')?'class="active"':''; ?> href="<?php echo base_url(); ?>summon/manage">Manage Summons</a></li>
                                <li><a <?php echo ($seg_ment_1=='summon' && $seg_ment_2=='paid')?'class="active"':''; ?> href="<?php echo base_url(); ?>summon/paid">Paid Summons</a></li>
                                <li><a <?php echo ($seg_ment_1=='summon' && $seg_ment_2=='pending')?'class="active"':''; ?> href="<?php echo base_url(); ?>summon/pending">Pending Summons</a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url(); ?>live/index"><i class="md md-map"></i> Live Tracking</a></li>
                        <li><a href="<?php echo base_url(); ?>suggestion/index"><i class="md md-map"></i> Suggestion Box</a></li>
                    </ul>
            <?php
                }
                else
                {
                    ?>
                    <ul class="main-menu">
                        <li><a href="<?php echo base_url(); ?>auth/index"><i class="md md-home"></i> Dashboard</a></li>       
                        <li><a href="<?php echo base_url(); ?>summon/mysummons"><i class="md md-account-balance"></i> My Summons</a></li>
                        <li><a href="<?php echo base_url(); ?>suggestion/index"><i class="md md-map"></i> Suggestion Box</a></li>
                    </ul>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</aside>