<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo ( isset( $title ) ) ? $title : APPLICATION_NAME; ?></title>

        <!-- Vendor CSS -->
        <link href="<?php echo base_url(); ?>vendors/fullcalendar/fullcalendar.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendors/animate-css/animate.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendors/sweet-alert/sweet-alert.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendors/material-icons/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendors/socicon/socicon.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>img/icons/favicon.ico" />
        <link href="<?php echo base_url(); ?>vendors/summernote/summernote.css" rel="stylesheet">
        <!-- CSS -->
        <link href="<?php echo base_url(); ?>css/app.min.1.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/app.min.2.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/dataTables.bootstrap.css" rel="stylesheet">
        

        <script src="<?php echo base_url(); ?>js/jquery-2.1.1.min.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>js/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>js/datatables/dataTables.bootstrap.js"></script>
        
        <script src="<?php echo base_url(); ?>js/jQuery.dtplugin.js"></script>
        <script src="<?php echo base_url(); ?>js/jqBarGraph.1.1.min.js"></script>
        
        <script type="text/javascript" src="<?php echo base_url(); ?>/js/canvasjs.min.js"></script>
        <script type="text/javascript">
  window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer",
    {
      title:{
        text: "Olympic Medals of all Times (till 2012 Olympics)"
      },
      animationEnabled: true,
      legend: {
        cursor:"pointer",
        itemclick : function(e) {
          if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
              e.dataSeries.visible = false;
          }
          else {
              e.dataSeries.visible = true;
          }
          chart.render();
        }
      },
      axisY: {
        title: "Medals"
      },
      toolTip: {
        shared: true,  
        content: function(e){
          var str = '';
          var total = 0 ;
          var str3;
          var str2 ;
          for (var i = 0; i < e.entries.length; i++){
             var  str1 = "<span style= 'color:"+e.entries[i].dataSeries.color + "'> " + e.entries[i].dataSeries.name + "</span>: <strong>"+  e.entries[i].dataPoint.y + "</strong> <br/>" ; 
            total = e.entries[i].dataPoint.y + total;
            str = str.concat(str1);
          }
          str2 = "<span style = 'color:DodgerBlue; '><strong>"+e.entries[0].dataPoint.label + "</strong></span><br/>";
          str3 = "<span style = 'color:Tomato '>Total: </span><strong>" + total + "</strong><br/>";
          
          return (str2.concat(str)).concat(str3);
        }

      },
      data: [
      {        
        type: "bar",
        showInLegend: true,
        name: "Gold",
        color: "gold",
        dataPoints: [
        { y: 198, label: "Italy"},
        { y: 201, label: "China"},
        { y: 202, label: "France"},        
        { y: 236, label: "Great Britain"},        
        { y: 395, label: "Soviet Union"},        
        { y: 957, label: "USA"}        


        ]
      },
      {        
        type: "bar",
        showInLegend: true,
        name: "Silver",
        color: "silver",          
        dataPoints: [
        { y: 166, label: "Italy"},
        { y: 144, label: "China"},
        { y: 223, label: "France"},        
        { y: 272, label: "Great Britain"},        
        { y: 319, label: "Soviet Union"},        
        { y: 759, label: "USA"}        


        ]
      },
      {        
        type: "bar",
        showInLegend: true,
        name: "Bronze",
        color: "#A57164",
        dataPoints: [
        { y: 185, label: "Italy"},
        { y: 128, label: "China"},
        { y: 246, label: "France"},        
        { y: 272, label: "Great Britain"},        
        { y: 296, label: "Soviet Union"},        
        { y: 666, label: "USA"}    

        ]
      }

      ]
    });

chart.render();
}
</script>

    </head>
    <body>
        
        <?php include('header.php'); ?>
        <section id="main">
            <?php include('sidebar.php'); ?>
            
            <section id="content">
                <div class="container">
                        <?php echo ( isset( $content ) ) ? $content : ''; ?>
                </div>
            </section>
        </section>
        
        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1 class="c-white">IE SUCKS!</h1>
                <p>You are using an outdated version of Internet Explorer, upgrade to any of the following web browser <br/>in order to access the maximum functionality of this website. </p>
                <ul class="iew-download">
                    <li>
                        <a href="http://www.google.com/chrome/">
                            <img src="img/browsers/chrome.png" alt="">
                            <div>Chrome</div>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.mozilla.org/en-US/firefox/new/">
                            <img src="img/browsers/firefox.png" alt="">
                            <div>Firefox</div>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.opera.com">
                            <img src="img/browsers/opera.png" alt="">
                            <div>Opera</div>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.apple.com/safari/">
                            <img src="img/browsers/safari.png" alt="">
                            <div>Safari</div>
                        </a>
                    </li>
                    <li>
                        <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                            <img src="img/browsers/ie.png" alt="">
                            <div>IE (New)</div>
                        </a>
                    </li>
                </ul>
                <p>Upgrade your browser for a Safer and Faster web experience. <br/>Thank you for your patience...</p>
            </div>   
        <![endif]-->
        
        <!-- Javascript Libraries -->
        
        
        <script src="<?php echo base_url(); ?>vendors/flot/jquery.flot.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/flot/jquery.flot.resize.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/flot/plugins/curvedLines.js"></script>
        <script src="<?php echo base_url(); ?>vendors/sparklines/jquery.sparkline.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/easypiechart/jquery.easypiechart.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/bootstrap-select/bootstrap-select.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/fullcalendar/lib/moment.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/fullcalendar/fullcalendar.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/simpleWeather/jquery.simpleWeather.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/auto-size/jquery.autosize.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/nicescroll/jquery.nicescroll.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/waves/waves.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/summernote/summernote.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/sweet-alert/sweet-alert.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
        <script src="<?php echo base_url(); ?>js/flot-charts/curved-line-chart.js"></script>
        <script src="<?php echo base_url(); ?>js/flot-charts/line-chart.js"></script>
        <script src="<?php echo base_url(); ?>js/charts.js"></script>
        <script src="<?php echo base_url(); ?>js/functions.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.tagsinput.js"></script>
        <script src="<?php echo base_url(); ?>vendors/bootgrid/jquery.bootgrid.min.js"></script>
        <script src="<?php echo base_url(); ?>js/demo.js"></script>
        <script src="<?php echo base_url(); ?>vendors/fileinput/fileinput.min.js"></script>
        <script src="vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
<script type="text/javascript">
            /*
             * Notifications
             */
            function notify(from, align, icon, type, animIn, animOut){
                $.growl({
                    icon: icon,
                    title: ' Bootstrap Growl ',
                    message: 'Turning standard Bootstrap alerts into awesome notifications',
                    url: ''
                },{
                        element: 'body',
                        type: type,
                        allow_dismiss: true,
                        placement: {
                                from: from,
                                align: align
                        },
                        offset: {
                            x: 20,
                            y: 85
                        },
                        spacing: 10,
                        z_index: 1031,
                        delay: 2500,
                        timer: 1000,
                        url_target: '_blank',
                        mouse_over: false,
                        animate: {
                                enter: animIn,
                                exit: animOut
                        },
                        icon_type: 'class',
                        template: '<div data-growl="container" class="alert" role="alert">' +
                                        '<button type="button" class="close" data-growl="dismiss">' +
                                            '<span aria-hidden="true">&times;</span>' +
                                            '<span class="sr-only">Close</span>' +
                                        '</button>' +
                                        '<span data-growl="icon"></span>' +
                                        '<span data-growl="title"></span>' +
                                        '<span data-growl="message"></span>' +
                                        '<a href="#" data-growl="url"></a>' +
                                    '</div>'
                });
            };
            
            $('.notifications > div > .btn').click(function(e){
                e.preventDefault();
                var nFrom = $(this).attr('data-from');
                var nAlign = $(this).attr('data-align');
                var nIcons = $(this).attr('data-icon');
                var nType = $(this).attr('data-type');
                var nAnimIn = $(this).attr('data-animation-in');
                var nAnimOut = $(this).attr('data-animation-out');
                
                notify(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut);
            });


            /*
             * Dialogs
             */

            //Basic
            $('#sa-basic').click(function(){
                swal("Here's a message!");
            });

            //A title with a text under
            $('#sa-title').click(function(){
                swal("Here's a message!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis")
            });

            //Success Message
            $('#sa-success').click(function(){
                swal("Good job!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis", "success")
            });

            //Warning Message
            $('#sa-warning').click(function(){
                swal({   
                    title: "Are you sure?",   
                    text: "You will not be able to recover this imaginary file!",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "Yes, delete it!",   
                    closeOnConfirm: false 
                }, function(){   
                    swal("Deleted!", "Your imaginary file has been deleted.", "success"); 
                });
            });
            
            //Parameter
            $('#sa-params').click(function(){
                swal({   
                    title: "Are you sure?",   
                    text: "You will not be able to recover this imaginary file!",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "Yes, delete it!",   
                    cancelButtonText: "No, cancel plx!",   
                    closeOnConfirm: false,   
                    closeOnCancel: false 
                }, function(isConfirm){   
                    if (isConfirm) {     
                        swal("Deleted!", "Your imaginary file has been deleted.", "success");   
                    } else {     
                        swal("Cancelled", "Your imaginary file is safe :)", "error");   
                    } 
                });
            });

            //Custom Image
            $('#sa-image').click(function(){
                swal({   
                    title: "Sweet!",   
                    text: "Here's a custom image.",   
                    imageUrl: "img/thumbs-up.png" 
                });
            });

            //Auto Close Timer
            $('#sa-close').click(function(){
                 swal({   
                    title: "Auto close alert!",   
                    text: "I will close in 2 seconds.",   
                    timer: 2000,   
                    showConfirmButton: false 
                });
            });

        </script>
     
        
    </body>
</html>