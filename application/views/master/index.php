<style type="text/css">
    .customColumn
    {
        width: 30%;
    }
</style>
<?php
if(isset($value) && !empty($value))
{
    foreach($value->result() as $row)
    {
        $st_id=$row->st_id;
        $st_type=$row->st_type;
        $st_desc=$row->st_desc;
        $charges=$row->charges;
        $st_status=$row->st_status;
    }
}
else
{
        $st_id=$this->input->post('st_id');
        $st_type=$this->input->post('st_type');
        $st_desc=$this->input->post('st_desc');
        $charges=$this->input->post('charges');
        $st_status=$this->input->post('st_status');
        $cat_status=$this->input->post('cat_status');     
}      
?>
<div class="card">
    <div class="card-header">
        <h2>Create Summon Types  <small>Summon Type Management</small></h2>
    </div>


    <?php if($this->session->flashdata('res')) { ?>
    <div class="alert alert-<?php echo $this->session->flashdata('res_type'); ?>">
        <?php echo $this->session->flashdata('res'); ?>
    </div>
    <?php } ?>

    <div class="card-body card-padding">
        <?php echo form_open("master/types");?>

        <div class="form-group <?php echo (form_error('st_type'))?'has-error':''; ?>">
            <div class="fg-line">
                <label class="control-label">Summon Type Name</label>
                <input type="text" autocomplete="off" name="st_type" class="form-control" placeholder="Title" value="<?php echo $st_type;?>">
            </div>
            <small class="help-block"><?php echo form_error( 'st_type'); ?></small>
        </div>

        <div class="form-group <?php echo (form_error('st_desc'))?'has-error':''; ?>">
            <div class="fg-line">
                <label class="control-label">Description</label>
                <textarea name="st_desc" class="form-control" placeholder="Description"><?php echo $st_desc; ?></textarea>
            </div>
            <small class="help-block"><?php echo form_error( 'st_desc'); ?></small>
        </div>
        <div class="form-group <?php echo (form_error('charges'))?'has-error':''; ?>">
            <div class="fg-line">
                <label class="control-label">Charges</label>
                <input type="text" autocomplete="off" name="charges" class="form-control" placeholder="Charges" value="<?php echo $charges;?>">
            </div>
            <small class="help-block"><?php echo form_error( 'charges'); ?></small>
        </div>
        <input type="hidden" name="st_id" value="<?php echo $st_id;?>" />
        <button type="submit" name="add" class="btn btn-primary btn-lg waves-effect waves-button waves-float">
            <?php echo ($st_id=='')?'Create Summon Type':'Update Summon Type';?>
        </button>
        <?php echo form_close();?>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h2>Summon Type Management</h2>
    </div>

  
    <div class="table-responsive" tabindex="2" style="overflow: hidden; outline: none;">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Summon Type</th>
                    <th>Description</th>
                    <th>Charges</th>
                    <th>Operations</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i=1;
                foreach($results as $data) 
                {
                    ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $data->st_type; ?></td>
                    <td><?php echo $data->st_desc; ?></td>
                    <td>RM <?php echo $data->charges; ?></td>
                    <td>
                        <a href="<?php echo base_url().'master/edit/'.$data->st_id; ?>" class="btn btn-success btn-xs waves-effect waves-button waves-float"> Edit</a>
                        <a href="javascript:void(0);" data-catid="<?php echo $data->st_id; ?>" id="sa-warning"  class="btn btn-danger del btn-xs waves-effect waves-button waves-float"> Delete</a>
                    </td>
                </tr>
                <?php
                $i++;
                }
                ?>
            </tbody>
        </table>
    </div>

    <div class="card-footer text-center">
        <ul class="fw-footer pagination wizard">
            <?php echo $links; ?>
        </ul>
    </div>
   
</div>
<?php function get_status($status, $id, $url) { $ci=& get_instance(); $html='<span class="actions">' ; if($status==1) { $html .='&nbsp;&nbsp;&nbsp;<a href="' . base_url(). $url. 'changeStatus/'.$id. '/0" title="Active" onclick="getConfirmStatus(this.href)"><i class="glyphicon glyphicon-eye-open"></i> Active</a>'; } else { $html .='&nbsp;&nbsp;&nbsp;<a href="' . base_url(). $url. 'changeStatus/'.$id. '/1" title="Inactive" onclick="getConfirmStatus(this.href)"><i class="glyphicon glyphicon-eye-close"></i> InActive</a>'; } $html.='</span>' ; return $html; } ?>
<script type="text/javascript">
$(document).ready(function() {

    //Warning Message
    $('.del').click(function(){

        var catid= $(this).attr("data-catid");
        swal({   
            title: "Are you sure?",   
            text: "May be some of the summons are based on this type, if you delete this every data associated with this category will be deleted permenantly!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            closeOnConfirm: false 
        }, function(isConfirm){   
            if (isConfirm) 
            {     
                window.location.href="<?php echo base_url().'master/delete/'; ?>"+catid;
                swal("Deleted!", "Summon type has been deleted.", "success");   
            } 
            else 
            {     
                swal("Cancelled", "Operation Cancelled :)", "error");   
            }
        });
    });
});
</script>
<script>
function getConfirmStatus(href) {

    var x;
    var r = confirm("You Want Change Status Confirm!!");
    if (r == true) {
        location.replace(userid);
    }
}
</script>
