<div class="card">
    <div class="card-header">
        <h2>Suggestions Management - View All</h2>
    </div>

  
    <div class="table-responsive" tabindex="2" style="overflow: hidden; outline: none;">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Citizen</th>
                    <th>Suggestion Description</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i=1;
                foreach($results as $data) 
                {
                    ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $data->first_name,' ',$data->last_name; ?></td>
                    <td><?php echo $data->suggestion; ?></td>
                    <td><?php echo $data->sg_date; ?></td>
                </tr>
                <?php
                $i++;
                }
                ?>
            </tbody>
        </table>
    </div>

    <div class="card-footer text-center">
        <ul class="fw-footer pagination wizard">
            <?php echo $links; ?>
        </ul>
    </div>
   
</div>