<div class="card">
    <div class="card-header">
        <h2>Summons Management - View All</h2>
    </div>

  
    <div class="table-responsive" tabindex="2" style="overflow: hidden; outline: none;">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Citizen</th>
                    <th>Summon Type</th>
                    <th>Charges</th>
                    <th>Summon Description</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i=1;
                foreach($results as $data) 
                {
                    ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $data->first_name,' ',$data->last_name; ?></td>
                    <td><strong><?php echo $data->st_type; ?></strong><br><?php echo $data->st_desc; ?></td>
                    <td>MYR <?php echo $data->charges; ?></td>
                    <td>MYR <?php echo $data->summon_name; ?></td>
                    <td>
                        <?php
                        if($data->s_status=='1')
                        {
                            ?>
                            <button class="btn btn-danger btn-xs waves-effect waves-button waves-float">NOT PAID</button>
                            <?php
                        }
                        if($data->s_status=='2')
                        {
                            ?>
                            <button class="btn btn-success btn-xs waves-effect waves-button waves-float">PAID</button>
                            <?php
                        }
                        
                        ?>
                        
                    </td>
                </tr>
                <?php
                $i++;
                }
                ?>
            </tbody>
        </table>
    </div>

    <div class="card-footer text-center">
        <ul class="fw-footer pagination wizard">
            <?php echo $links; ?>
        </ul>
    </div>
   
</div>