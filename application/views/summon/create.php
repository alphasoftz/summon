<style type="text/css">
    .customColumn
    {
        width: 30%;
    }
</style>
<?php
if(isset($value) && !empty($value))
{
    foreach($value->result() as $row)
    {
        $sid=$row->sid;
        $summon_name=$row->summon_name;
        $summon_type=$row->summon_type;
        $summon_date=$row->summon_date;
        $s_status=$row->s_status;
    }
}
else
{
        $sid=$this->input->post('sid');
        $summon_name=$this->input->post('summon_name');
        $summon_type=$this->input->post('summon_type');
        $summon_date=$this->input->post('summon_date');
        $s_status=$this->input->post('s_status');
}      
?>
<div class="card">
    <div class="card-header">
        <h2>Create New Summon  <small></small></h2>
    </div>

    <?php if($this->session->flashdata('res')) { ?>
    <div class="alert alert-<?php echo $this->session->flashdata('res_type'); ?>">
        <?php echo $this->session->flashdata('res'); ?>
    </div>
    <?php } ?>

    <div class="card-body card-padding">
        <?php echo form_open("summon/create");?>

        <div class="form-group <?php echo (form_error('user_id'))?'has-error':''; ?>">
            <div class="fg-line">
                <label class="control-label">Summon for user</label>
                <div class="select">
                            <select class="form-control" name="user_id">
                                <option value="">-- Select User--</option>
                                <?php
                                foreach ($users as $key => $values) {
                                    ?>
                                    <option <?php echo ($values->id == $id)?'Selected':'';?> value="<?php echo $values->id; ?>"> <?php echo $values->first_name,' ',$values->last_name; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
            </div>
            <small class="help-block"><?php echo form_error( 'user_id'); ?></small>
        </div>
        <div class="form-group <?php echo (form_error('summon_type'))?'has-error':''; ?>">
            <div class="fg-line">
                <label class="control-label">Summon Type</label>
                <div class="select">
                            <select class="form-control" name="summon_type">
                                <option value="">-- Select Type--</option>
                                <?php
                                foreach ($types as $key => $values) {
                                    ?>
                                    <option <?php echo ($values->st_id == $st_id)?'Selected':'';?> value="<?php echo $values->st_id; ?>"> <?php echo $values->st_type; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
            </div>
            <small class="help-block"><?php echo form_error( 'summon_type'); ?></small>
        </div>

        <div class="form-group <?php echo (form_error('summon_name'))?'has-error':''; ?>">
            <div class="fg-line">
                <label class="control-label">Summon Description</label>
                <textarea name="summon_name" class="form-control" placeholder="Description"><?php echo $summon_name; ?></textarea>
            </div>
            <small class="help-block"><?php echo form_error( 'summon_name'); ?></small>
        </div>
        
        <input type="hidden" name="sid" value="<?php echo $sid;?>" />
        <button type="submit" name="add" class="btn btn-primary btn-lg waves-effect waves-button waves-float">
            <?php echo ($sid=='')?'Create Summon':'Update Summon';?>
        </button>
        <?php echo form_close();?>
    </div>
</div>
    <div class="card-footer text-center">
        <ul class="fw-footer pagination wizard">
            <?php echo $links; ?>
        </ul>
    </div>
   
</div>
<?php function get_status($status, $id, $url) { $ci=& get_instance(); $html='<span class="actions">' ; if($status==1) { $html .='&nbsp;&nbsp;&nbsp;<a href="' . base_url(). $url. 'changeStatus/'.$id. '/0" title="Active" onclick="getConfirmStatus(this.href)"><i class="glyphicon glyphicon-eye-open"></i> Active</a>'; } else { $html .='&nbsp;&nbsp;&nbsp;<a href="' . base_url(). $url. 'changeStatus/'.$id. '/1" title="Inactive" onclick="getConfirmStatus(this.href)"><i class="glyphicon glyphicon-eye-close"></i> InActive</a>'; } $html.='</span>' ; return $html; } ?>
<script type="text/javascript">
$(document).ready(function() {

    //Warning Message
    $('.del').click(function(){

        var catid= $(this).attr("data-catid");
        swal({   
            title: "Are you sure?",   
            text: "May be some of the summons are based on this type, if you delete this every data associated with this category will be deleted permenantly!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            closeOnConfirm: false 
        }, function(isConfirm){   
            if (isConfirm) 
            {     
                window.location.href="<?php echo base_url().'master/delete/'; ?>"+catid;
                swal("Deleted!", "Summon type has been deleted.", "success");   
            } 
            else 
            {     
                swal("Cancelled", "Operation Cancelled :)", "error");   
            }
        });
    });
});
</script>
<script>
function getConfirmStatus(href) {

    var x;
    var r = confirm("You Want Change Status Confirm!!");
    if (r == true) {
        location.replace(userid);
    }
}
</script>
