<div class="lc-block toggled" id="l-login">


            <div class="bp-header">
                    <h2><?php echo WEBSITE_NAME; ?></h2>
            </div>

            <div class="clearfix"></div>
            <?php echo form_open("auth/login");?>
            <div id="infoMessage"><?php echo $message;?></div>
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="md md-person"></i></span>
                <div class="fg-line">
                    <?php echo form_input($identity,TRUE,'placeholder="yourname@summon.com" class="form-control"');?>
                </div>
            </div>
            
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="md md-accessibility"></i></span>
                <div class="fg-line"><?php echo form_input($password,'','placeholder="Password" class="form-control"');?>
                </div>
            </div>
            
            <div class="clearfix"></div>
            
            <div class="checkbox">
                <label>
                    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
                    <i class="input-helper"></i>
                    <?php echo lang('login_remember_label', 'remember');?>
                </label>
            </div>
            <button type="submit" class="btn btn-login btn-danger btn-float"><i class="md md-arrow-forward"></i></button>
            <?php echo form_close();?>
            <!--<ul class="login-navigation">
                <li class="bgm-orange"><a  style="color:#fff" href="forgot_password"><?php echo lang('login_forgot_password');?></a></li>
            </ul>-->
            <br>
            <a href="register" class="btn btn-success">New user Registeration</a>
            <button class="btn btn-warning">Reset Password</button>
        </div>
