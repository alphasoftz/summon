<div class="lc-block toggled">
     <div class="bp-header">
                <img src="<?php echo base_url(); ?>/img/logo.png"><br>
            </div>

            <div class="clearfix"></div>
	<?php echo form_open("auth/forgot_password");?>
	<h1><?php echo lang('forgot_password_heading');?></h1>
    <p class="text-left"><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>
    <div id="infoMessage"><?php echo $message;?></div>
    <div class="input-group m-b-20">
        <span class="input-group-addon"><i class="md md-email"></i></span>
        <div class="fg-line">
        	<?php echo form_input($email,'','class="form-control" placeholder="Email Address"');?>
        </div>
    </div>
    <button type="submit" class="btn btn-login btn-danger btn-float"><i class="md md-arrow-forward"></i></button>
    
    <ul class="login-navigation">
        <li class="bgm-green"><a style="color:#fff" href="login">Login</a></li>
    </ul>
    <?php echo form_close();?>
</div>