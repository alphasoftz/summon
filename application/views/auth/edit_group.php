<div class="card">
<div class="card-header">
    <h2><?php echo lang('edit_group_heading');?>
    <small><?php echo lang('edit_group_subheading');?></small>
    </h2>
<?php
  if($this->session->flashdata('res'))
  {
      ?>
<div id="infoMessage"><?php echo $message;?></div>
<?php
  }
?>
<div class="card-body  card-padding" tabindex="0">
	<?php echo form_open(current_url());?>
	<div class="form-group fg-line">
      <label><?php echo lang('edit_group_name_label', 'group_name');?></label>
            <?php echo form_input($group_name,'','class="form-control"');?>
    </div>
    <div class="form-group fg-line">
      <label><?php echo lang('edit_group_desc_label', 'description');?></label>
            <?php echo form_input($group_description,'','class="form-control"');?>
    </div>
    <button type="submit" class="btn btn-primary btn-lg waves-effect waves-button waves-float"><?php echo lang('edit_group_submit_btn');?></button>
<?php echo form_close();?>
</div>
</div>