
<div class="card">
<style type="text/css">
	p{
		color: #333;
	}
</style>
<div class="card-header">
    <h2><?php echo lang('index_heading');?> <small><?php echo lang('index_subheading');?></small></h2>

    <ul class="actions">
        <li class="dropdown">
            <a href="#" data-toggle="dropdown" aria-expanded="true">
                <i class="md md-more-vert"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
                <li>
                    <?php echo anchor('auth/create_user', lang('index_create_user_link'))?>
                </li>
                <li>
                    <?php echo anchor('auth/create_group', lang('index_create_group_link'))?>
                </li>
            </ul>
        </li>
    </ul>
</div>
<?php
  if($this->session->flashdata('message'))
  {
      ?>
      <div class="alert alert-info" style="color:#333">
      	<?php echo $message;?>
      </div>
  <?php
  }
?>
<div class="card-body table-responsive" tabindex="0">

<table id="data-table-basic" class="table">
	<thead>
	<tr>
		<th data-column-id="<?php echo lang('index_fname_th');?>"><?php echo lang('index_fname_th');?></th>
		<th data-column-id="<?php echo lang('index_lname_th');?>"><?php echo lang('index_lname_th');?></th>
		<th data-column-id="<?php echo lang('index_email_th');?>"><?php echo lang('index_email_th');?></th>
		<th data-column-id="<?php echo lang('index_groups_th');?>"><?php echo lang('index_groups_th');?></th>
		<th data-column-id="<?php echo lang('index_status_th');?>"><?php echo lang('index_status_th');?></th>
		<th data-column-id="<?php echo lang('index_action_th');?>"><?php echo lang('index_action_th');?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($users as $user):?>
		<tr>
            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
			<td>
      
        <?php foreach ($user->groups as $group):?>
          <a class="badge <?php echo ($group->name=='admin')?'bgm-red':'bgm-orange'; ?>" role="menuitem" tabindex="-1" href="auth/edit_group/<?php echo $group->id; ?>"><?php echo htmlspecialchars(ucfirst($group->name),ENT_QUOTES,'UTF-8'); ?></a>
        <?php endforeach?>
      
					<?php //echo anchor("auth/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;?><br />
        
			</td>
			<td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, lang('index_active_link')) : anchor("auth/activate/". $user->id, lang('index_inactive_link'));?></td>
			<td><?php echo anchor("auth/edit_user/".$user->id, 'Edit') ;?></td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>

</div>