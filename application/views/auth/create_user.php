<div class="card">
<div class="card-header">
    <h2><?php echo lang('create_user_heading');?> <small><?php echo lang('create_user_subheading');?></small></h2>

    <ul class="actions">
        <li class="dropdown">
            <a href="#" data-toggle="dropdown" aria-expanded="true">
                <i class="md md-more-vert"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
                <li>
                    <?php echo anchor('auth/create_user', lang('index_create_user_link'))?>
                </li>
                <li>
                    <?php echo anchor('auth/create_group', lang('index_create_group_link'))?>
                </li>
            </ul>
        </li>
    </ul>
    
    
</div>
<?php
  if(!empty($message))
  {
      ?>
  <div id="infoMessage"><?php echo $message;?></div>
  <?php
  }
?>

<div class="card-body  card-padding" tabindex="0">
<?php echo form_open("auth/create_user");?>
      <div class="form-group fg-line">
      <label><?php echo lang('create_user_fname_label', 'first_name');?></label>
      <?php echo form_input($first_name,'','class="form-control"');?>
      </div>
      <div class="form-group fg-line">
      <label><?php echo lang('create_user_lname_label', 'last_name');?></label>
            <?php echo form_input($last_name,'','class="form-control"');?>
      </div>
      <div class="form-group fg-line">
      <label><?php echo lang('create_user_company_label', 'company');?></label>
            <?php echo form_input($company,'','class="form-control"');?>
      </div>
      <div class="form-group fg-line">
      <label><?php echo lang('create_user_email_label', 'email');?> </label>
            <?php echo form_input($email,'','class="form-control"');?>
      </div>
      <div class="form-group fg-line">
      <label>
            <?php echo lang('create_user_phone_label', 'phone');?> </label>
            <?php echo form_input($phone,'','class="form-control"');?>
      </div>
      <div class="form-group fg-line">
      <label>
            <?php echo lang('create_user_password_label', 'password');?> </label>
            <?php echo form_input($password,'','class="form-control"');?>
      </div>
      <div class="form-group fg-line">
      <label>
            <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> </label>
            <?php echo form_input($password_confirm,'','class="form-control"');?>
      </div>

      <div class="form-group fg-line">
      <label>IC Number </label>
            <?php echo form_input($ic_number,'','class="form-control"');?>
      </div>
      
      <button type="submit" class="btn btn-primary btn-lg waves-effect waves-button waves-float"><?php echo lang('create_user_submit_btn');?></button>
      
<?php echo form_close();?>
</div>
</div>