<div class="card">
<div class="card-header">
    <h2>
        <?php echo lang('edit_user_heading');?> 
        <small><?php echo lang('edit_user_subheading');?></small>
    </h2>
</div>
<?php
  if($this->session->flashdata('res'))
  {
      ?>
      <div id="infoMessage"><?php echo $message;?></div>
       <?php
  }
?>
<div class="card-body  card-padding" tabindex="0">
  <?php echo form_open(uri_string());?>
    <div class="form-group fg-line">
      <label><?php echo lang('edit_user_fname_label', 'first_name');?></label>
      <?php echo form_input($first_name,'','class="form-control"');?>
    </div>
    <div class="form-group fg-line">
      <label><?php echo lang('edit_user_lname_label', 'last_name');?></label>
            <?php echo form_input($last_name,'','class="form-control"');?>
    </div>
    <div class="form-group fg-line">
      <label><?php echo lang('edit_user_company_label', 'company');?></label>
            <?php echo form_input($company,'','class="form-control"');?>
    </div>
    <div class="form-group fg-line">
      <label><?php echo lang('edit_user_phone_label', 'phone');?></label>
            <?php echo form_input($phone,'','class="form-control"');?>
    </div>
    <div class="form-group fg-line">
      <label><?php echo lang('edit_user_password_label', 'password');?></label>
            <?php echo form_input($password,'','class="form-control"');?>
    </div>
    <div class="form-group fg-line">
      <label><?php echo lang('edit_user_password_confirm_label', 'password_confirm');?></label>
            <?php echo form_input($password_confirm,'','class="form-control"');?>
    </div>
    <?php if ($this->ion_auth->is_admin()): ?>
    <p class="c-black f-500 m-b-20"><?php echo lang('edit_user_groups_heading');?></p>

          <?php foreach ($groups as $group):?>
            <?php
                  $gID=$group['id'];
                  $checked = null;
                  $item = null;
                  foreach($currentGroups as $grp) {
                      if ($gID == $grp->id) {
                          $checked= ' checked="checked"';
                      break;
                      }
                  }
              ?>
            <div class="checkbox m-b-15">
                <label>
                    <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>" <?php echo $checked;?>>
                    <i class="input-helper"></i>
                    <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
                </label>
            </div>
          <?php endforeach?>

      <?php endif ?>

      <?php echo form_hidden('id', $user->id);?>
      <?php echo form_hidden($csrf); ?>

      <p><?php echo form_submit('submit', lang('edit_user_submit_btn'));?></p>

<?php echo form_close();?>
<div class="card">