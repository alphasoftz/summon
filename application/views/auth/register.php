<div class="lc-block toggled" id="l-login" style="text-align:left">
<h4>Register</h4>
<?php echo form_open("auth/register");?>

<?php if($this->session->flashdata('res')) { ?>
    <div class="alert alert-<?php echo $this->session->flashdata('res_type'); ?>">
        <?php echo $this->session->flashdata('res'); ?>
    </div>
    <?php } ?>


    <div class="form-group fg-line">
      <label><?php echo lang('create_user_fname_label', 'first_name');?></label>
      <?php echo form_input($first_name,'','class="form-control"');?>
      </div>
      <div class="form-group fg-line">
      <label><?php echo lang('create_user_lname_label', 'last_name');?></label>
            <?php echo form_input($last_name,'','class="form-control"');?>
      </div>
      
      <div class="form-group fg-line">
      <label><?php echo lang('create_user_email_label', 'email');?> </label>
            <?php echo form_input($email,'','class="form-control"');?>
      </div>
      <div class="form-group fg-line">
      <label>
            <?php echo lang('create_user_phone_label', 'phone');?> </label>
            <?php echo form_input($phone,'','class="form-control"');?>
      </div>
      <div class="form-group fg-line">
      <label>
            <?php echo lang('create_user_password_label', 'password');?> </label>
            <?php echo form_input($password,'','class="form-control"');?>
      </div>
      <div class="form-group fg-line">
      <label>
            <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> </label>
            <?php echo form_input($password_confirm,'','class="form-control"');?>
      </div>

      <div class="form-group fg-line">
      <label>IC No</label>
            <?php echo form_input($ic,'','class="form-control"');?>
      </div>
      
      <button type="submit" class="btn btn-primary btn-lg waves-effect waves-button waves-float">Create Account</button>
<?php echo form_close();?>
</div>