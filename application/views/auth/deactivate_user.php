<div class="card">
<div class="card-header">
    <h2>
        <?php echo lang('deactivate_heading');?> 
        <small><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></small>
    </h2>
</div>

<div class="card-body  card-padding" tabindex="0">
<?php echo form_open("auth/deactivate/".$user->id);?>

<div class="radio m-b-15">
    <label>
        <input type="radio" name="confirm" value="yes" checked="checked">
        <i class="input-helper"></i>
        <?php echo lang('deactivate_confirm_y_label', 'confirm');?>
    </label>
</div>
<div class="radio m-b-15">
    <label>
        <input type="radio" name="confirm" value="no">
        <i class="input-helper"></i>
        <?php echo lang('deactivate_confirm_n_label', 'confirm');?>
    </label>
</div>
  
  <?php echo form_hidden($csrf); ?>
  <?php echo form_hidden(array('id'=>$user->id)); ?>
<button type="submit" class="btn btn-primary btn-lg waves-effect waves-button waves-float"><?php echo lang('deactivate_submit_btn');?></button>
 <!-- <p><?php echo form_submit('submit', lang('deactivate_submit_btn'));?></p>-->

<?php echo form_close();?>
</div>
</div>