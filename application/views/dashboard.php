<div class="card">
    <div class="card-header">
        <h2>Dashboard  <small>Stats &amp; Notifications</small></h2>
    </div>
    <div class="card-body   card-padding">
        <div class="mini-charts">
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="mini-charts-item bgm-amber">
                        <div class="clearfix">
                            <div class="chart"><h1 style="font-size:44px;"><i class="md c-white md-account-child"></i></h1></div>
                            <div class="count">
                                <small>Total Users</small>
                                <h2><?php echo $ap_bank; ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-4">
                    <div class="mini-charts-item bgm-green">
                        <div class="clearfix">
                            <div class="chart"><h1 style="font-size:44px;"><i class="md c-white md-assignment"></i></h1></div>
                            <div class="count">
                                <small>Paid Summons</small>
                                <h2><?php echo $tot_paid; ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
                <div class="col-sm-6 col-md-4">
                    <div class="mini-charts-item bgm-brown">
                        <div class="clearfix">
                            <div class="chart"><h1 style="font-size:44px;"><i class="md c-white md-assignment"></i></h1></div>
                            <div class="count">
                                <small>Total Summons</small>
                                <h2><?php echo $total; ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                <div id="divForGraph">
                </div>
                </div>
            </div>
        </div>
        

   <script type="text/javascript">
/*
 // Multi bar chart
      arrayOfData = new Array(
        [[65,60],'January'],
        [[43,63],'February'],
        [[59,67],'March'],
        [[78,70],'April'],
        [[87,93],'May'],
        [[93,90],'June'],
        [[77,84],'July'],
        [[85,72],'Ogos']
        
      );*/

      arrayOfData = new Array(

        <?php
        $this->load->model('statistics_model');
        for ($i=1; $i <=12 ; $i++) 
        { 
            
            $monthName = date('F', mktime(0, 0, 0, $i, 10));
            //echo $i,date('Y').'<br>';
             $paid = $this->statistics_model->paid($i,date('Y'));
             $all = $this->statistics_model->all($i,date('Y'));
             $notpaid = $this->statistics_model->notpaid($i,date('Y'));

             $paid=$paid['count(*)'];
             $all=$all['count(*)'];
             $notpaid=$notpaid['count(*)'];
           
                ?>
                [[<?php echo ($paid=='')?'0':$paid; ?>,<?php echo ($notpaid=='')?'0':$notpaid; ?>,<?php echo ($all=='')?'0':$all; ?>],'<?php echo $monthName; ?>'],
                <?php
            
        }
        ?>
        
      );


   $('#divForGraph').jqBarGraph({ colors: ['#3D0017','#6B0E1D'],
    legends: ['Paid','Not paid','All'],
    legend: true,
    data: arrayOfData,
    type:'multi', 
    postfix:'',
    width:1000,
    title:'<h3>Strategies <?php echo date('Y'); ?> - Summon Statistics<br><br></h3>',
    colors: ['#4caf50','#f44336','#ffc107'] });

   </script>
    </div>
</div>