<?php 
/*
 * function that generate the action buttons edit, delete
 * This is just showing the idea you can use it in different view or whatever fits your needs
 */
function get_buttons($id, $url, $ext='', $delid='')
{
    $ci= & get_instance();
    $html='<span class="actions">';
    
	
	if($ext == 'seats')
	{
		$html .='&nbsp;&nbsp;&nbsp;<a href="'.base_url().$url.'ManagePax/'.$id.'" title="Pax Management">
								   	<i class="glyphicon glyphicon-list"></i>
								   </a>';
	}
	
	if($ext == 'details')
	{
		$html .='&nbsp;&nbsp;&nbsp;<a href="'.  base_url().$url.'details/'.$id.'"><i class="glyphicon glyphicon-list"></i></a>';
	}
	
	if($ext == 'print')
	{
		$html .='&nbsp;&nbsp;&nbsp;<a href="'.  base_url(). $url.'printcard/'.$id.'"><i class="glyphicon glyphicon-print"></i></a>';
	}

	if($delid !='')
	{
		$html .='&nbsp;&nbsp;&nbsp;<a href="'.  base_url(). $url.'edit/'.$id.'"><i class="glyphicon glyphicon-pencil"></i></a>';
		$html .='&nbsp;&nbsp;&nbsp;<a href="'.  base_url(). $url.'delete/'.$delid.'" onclick="getConfirm(this.href)"><i class="glyphicon glyphicon-trash"></i></a>';
	}
	else
	{
		$html .='&nbsp;&nbsp;&nbsp;<a href="'.  base_url(). $url.'edit/'.$id.'"><i class="glyphicon glyphicon-pencil"></i></a>';
		$html .='&nbsp;&nbsp;&nbsp;<a href=javascript:getConfirm("'.  base_url(). $url.'delete/'.$id.'");><i class="glyphicon glyphicon-trash"></i></a>';
	}
	
	$html.='</span>';
    return $html;
}

function get_select($id)
{
    $ci= & get_instance();
    $html='<span class="actions">
			<input type="checkbox" name="apply_id[]" value="'.$id.'" onclick="show_submit_button()">
			<input type="hidden" name="list_apply_id[]" value="'.$id.'">
		</span>';
	return $html;
}


function get_edit_buttons($id, $url, $ext='', $delid='')
{
	$html .='&nbsp;&nbsp;&nbsp;<a href="'.base_url(). $url.'edit/'.$id.'"><i class="glyphicon glyphicon-pencil"></i></a>';
    return $html;
}

function get_po_details($id, $url, $po_status)
{
    $ci= & get_instance();
    $html='<span class="actions">';
	$html .='&nbsp;<a href="'.  base_url(). $url.'receivepo/'.$id.'" title="View Details"><i class="glyphicon glyphicon-list"></i></a>';
	
	if($po_status==1)
	{
		$html .='<a href="'.  base_url(). $url.'edit/'.$id.'" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>';
		$html .='<a href=javascript:getConfirm("'.  base_url(). $url.'delete/'.$id.'"); title="Delete"><i class="glyphicon glyphicon-trash"></i></a>';
	}
	
	$html.='</span>';
    return $html;
}

function get_recipt($id, $url, $ext='', $receipt_status)
{
    $ci= & get_instance();
    $html='<span class="actions">';
	

		$html.='<br /><a href="'.base_url().$url.'generate/'.$id.'/Pdf" title="Receipt PDF" target="_blank">
						<i class="fa fa-file-pdf-o"></i></a>&nbsp;';
		$html.='&nbsp;<a href="'.base_url().$url.'generate/'.$id.'/sendPdf" title="Receipt Mail" target="_blank">
						<i class="fa fa-envelope-o"></i></a> &nbsp;';

	$html.='</span>';
    return $html;
}

function get_statusLable($status)
{
    $html='<span class="label label-'.$status.'">';
	$html .=$status;
	$html.='</span>';
	
    return $html;
}

function get_status_icon($status)
{
	if($status == 0)
	{
		$html='<i class="fa fa-minus" style="color:red;"></i>';
	}
	elseif($status == 1)
	{
		$html='<i class="fa fa-check" style="color:green"></i>';
	}	
	
    return $html;
}
function get_empstatus($status,$id)
{
	if($status == 0)
	{
		$html='<span class="label label-danger">In Active</span>';
	}
	elseif($status == 1)
	{
		$html='<span class="label label-success">Active</span>';
	}	
    return $html;
}

function get_invoiceLable($status)
{
	if($status == 0)
	{
		$html='<span class="label label-danger">Not Paid</span>';
	}
	elseif($status == 1)
	{
		$html='<span class="label label-warning">Pending</span>';
	}	
	elseif($status == 5)
	{
		$html='<span class="label label-success">Paid</span>';
	}
    return $html;
}

function get_status($status, $id, $url)
{
    $ci= & get_instance();
    $html='<span class="actions">';
    
	if($status==1)
	{
   		$html .='&nbsp;&nbsp;&nbsp;<a href="'.  base_url(). $url.'changeStatus/'.$id.'/0" title="Active" onclick="getConfirmStatus(this.href)"><i class="glyphicon glyphicon-eye-open"></i> Active</a>';
	}else
	{
   		$html .='&nbsp;&nbsp;&nbsp;<a href="'.  base_url(). $url.'changeStatus/'.$id.'/1" title="In-Active" onclick="getConfirmStatus(this.href)"><i class="glyphicon glyphicon-eye-close"></i> In-Active</a>';
	}

	$html.='</span>';
    return $html;
}

function getChangeStatus($id, $url, $applicant)
{
    $ci= & get_instance();
    $html='<span class="actions">';
    
   		$html .='&nbsp;&nbsp;&nbsp;
			<a href="'.  base_url(). $url.'updateStatus/'.$id.'/'.$applicant.'" title="Update Status">
				<i class="glyphicon glyphicon-thumbs-up"></i> 
			</a>';

	$html.='</span>';
    return $html;
}

function get_issue_status($status, $id, $url)
{
    $ci= & get_instance();
    $html='<span class="actions">';
    
	if($status==1)
	{
   		$html .='&nbsp;&nbsp;&nbsp;<a href="'.  base_url(). $url.'cardIssued/'.$id.'/0" title="Issued" onclick="getConfirmStatus(this.href)"><i class="glyphicon glyphicon-thumbs-up"></i> Issued</a>';
	}else
	{
   		$html .='&nbsp;&nbsp;&nbsp;<a href="'.  base_url(). $url.'cardIssued/'.$id.'/1" title="Not Issue" onclick="getConfirmStatus(this.href)"><i class="glyphicon glyphicon-thumbs-down"></i> No</a>';
	}

	$html.='</span>';
    
    return $html;
}


function get_image_tag($url)
{
    $ci= & get_instance();
	$html .='<a href="'.  base_url(). $url.'" target="_blank"><img src="'.base_url().$url.'" height="40" title="Edit"/></a>';
	return $html;
}

function get_dateformat($datevalue)
{
    $ci= & get_instance();	
	if($datevalue == '0000-00-00')
	{
		return '';
	}else
	{
		return date('d-m-Y', strtotime($datevalue));
	}
}

function get_dateTimeFormat($dateTime)
{
	if($dateTime == '0000-00-00 00:00:00')
	{
		return '';
	}else
	{
		return date('d-m-Y', strtotime($dateTime)).' <br /><span style="color:blue;">'.date('H:s A', strtotime($dateTime)).'</span>';
	}
}

function change_dateformat($datevalue)
{
	if($datevalue == '0000-00-00')
	{
		return '';
	}else
	{
		return date('d-m-Y', strtotime($datevalue));
	}
}

function get_price_name($id)
{
	$sql=mysql_query("SELECT * FROM `currency` WHERE `cur_id`='".$id."'") or die(mysql_error());
	$exe=mysql_fetch_array($sql);
	return $exe['cur_name'];
	
}

function get_email_status($status)
{
	if($status==1)
	{
		 $html='<span class="label label-success">';
		$html .='YES';
		$html.='</span>';
		return $html;
	}
	elseif($status==0)
	{
		 $html='<span class="label label-danger">';
		$html .='NO';
		$html.='</span>';
		return $html;
	}
}

function check_buttons($id, $url, $userlevel)
{
    $ci= & get_instance();
	if($userlevel==9)
	{
		$html='<span class="actions">';
		$html .='<a href="'.  base_url(). $url.'operation?id='.$id.'"><img src="'.  base_url().'assets/images/edit.png" width="16" title="Edit"/></a> &nbsp;';
		$html .='<a href="'.  base_url(). $url.'delete/'.$id.'"><img src="'.  base_url().'assets/images/delete.png" width="16" title="delete"/></a>';
		$html.='</span>';
	}
    return $html;
}


function get_combine_data($data1, $data2, $deli)
{
    $ci= & get_instance();	
	return $data1.''.$deli.' '.$data2;
}

function amountCalculations($cur, $amount, $exRate)
{
    $ci= & get_instance();	
	return $cur.' '.round($amount*$exRate);
}

function getMarginAmt($cur, $sellingAmt, $sellingexRate, $buyingAmt, $buyingexRate)
{
    $ci= & get_instance();	
	$sellingAmount = round($sellingAmt*$sellingexRate);
	$buyingAmount = round($buyingAmt*$buyingexRate);
	$margin = $sellingAmount - $buyingAmount;
	return $cur.' '.$margin;
}

function getMarginPercent($sellingAmt, $sellingexRate, $buyingAmt, $buyingexRate)
{
    $ci= & get_instance();	
	$sellingAmount = round($sellingAmt*$sellingexRate);
	$buyingAmount = round($buyingAmt*$buyingexRate);
	$margin = $sellingAmount - $buyingAmount;
	$percentage = $margin / $sellingAmount * 100;

	return ' <strong style="color:red;">'.round($percentage, 2).'%</strong>';
}

function get_visatype_data($data1, $data2, $data3, $deli)
{
    $ci= & get_instance();	
	return $data1.$deli.$data2.'Days'.$deli.$data3;
}


function get_dc_status($status)
{
	switch($status)
	{
		case 1:
			return '<strong style="color:#0018BD;">DC with JJ Form</strong>';
			break;
		case 5:
			return '<strong style="color:#9B00BD;">DC with Invoice</strong>';	
			break;
	}
}


function no_to_words($no)
{   
	 $words = array('0'=> '' , '1'=> 'One' , '2'=> 'Two' , '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six', '7' => 'Seven', '8' => 'Eight', '9' => 'Nine', '10' => 'Ten', '11' => 'Eleven','12' => 'Twelve','13' => 'Thirteen','14' => 'Fouteen','15' => 'Fifteen','16' => 'Sixteen','17' => 'Seventeen','18' => 'Eighteen','19' => 'Nineteen','20' => 'Twenty','30' => 'Thirty','40' => 'Fourty','50' => 'Fifty','60' => 'Sixty','70' => 'Seventy','80' => 'Eighty','90' => 'Ninty','100' => 'Hundred','1000' => 'Thousand','100000' => 'Lakh','10000000' => 'Crore');
    if($no == 0)
        return ' ';
    else {
	$novalue='';
	$highno=$no;
	$remainno=0;
	$value=100;
	$value1=1000; 

		while($no>=100)    
		{
			if(($value <= $no) &&($no  < $value1))   
			 {
			$novalue=$words["$value"];
			$highno = (int)($no/$value);
			$remainno = $no % $value;
			break;
			}
			$value= $value1;
			$value1 = $value * 100;
		}       
         
		  if(array_key_exists("$highno",$words))
		  {
             return $words["$highno"]." ".$novalue." ".no_to_words($remainno);
		  }
          else 
		  {
			  if($highno >= 20)
			  {
				 $unit=$highno%10;
				 $ten =(int)($highno/10)*10;            
				 return $words["$ten"]." ".$words["$unit"]." ".$novalue." ".no_to_words($remainno);
			  }else
			  {
				  return $words[$highno];
			  }
           }
    }
   	
}

function currency_no_to_words($no)
{   
	$number = explode('.',number_format($no,2)); 
	$decimal = $number[1];
	
	 $words = array('0'=> '' ,'1'=> 'One' ,'2'=> 'Two' ,'3' => 'Three','4' => 'Four','5' => 'Five','6' => 'Six','7' => 'Seven','8' => 'Eight','9' => 'Nine','10' => 'Ten','11' => 'Eleven','12' => 'Twelve','13' => 'Thirteen','14' => 'Fouteen','15' => 'Fifteen','16' => 'Sixteen','17' => 'Seventeen','18' => 'Eighteen','19' => 'Nineteen','20' => 'Twenty','30' => 'Thirty','40' => 'Fourty','50' => 'Fifty','60' => 'Sixty','70' => 'Seventy','80' => 'Eighty','90' => 'Ninty','100' => 'Hundred','1000' => 'Thousand','100000' => 'Lakh','10000000' => 'Crore');
	
	if($decimal >= 20)
	{
		$unit=$decimal%10;
		$ten =(int)($decimal/10)*10;
		$paisa = $words["$ten"]." ".$words["$unit"].' paisa';
	}elseif($decimal > 0)
	{
		$paisa = $words[$decimal].' paisa';
	}

 return no_to_words($no).' rupees '.$paisa.' only';
}



function get_rpo_button($id,$url,$po_status)
{
    $ci= & get_instance();
    $html='<span class="actions">';
	if($po_status==0)
	{
	    $html .='<a href="'.  base_url(). $url.'inspection/'.$id.'" title="Inspction"><i class="glyphicon glyphicon-check"></i></a> &nbsp;';
	    $html .='<a href="'.  base_url(). $url.'operation/'.$id.'" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a> &nbsp;';
	}
		$html.='</span>';	
	
    return $html;
}

function get_po_payment($po_id, $url,$payment_status,$supplier_id)
{
   			$ci= & get_instance();
		$html='<span class="actions">';
		if($payment_status<5)
		{
			if($supplier_id !='')
			{
				$html .='<input type="checkbox" name="po_nos[]" value="'.$po_id.'" class="form-control" style="width: 17px;" onclick="proceedToInvoice()">';
			}
		}
	
	$html.='</span>';
    return $html;
}

function get_po_type($po_for)
{
	 $ci= & get_instance();

	if($po_for==1)
	{
		$html = '<span class="label label-success">Spare</span>';
	}
	elseif($po_for==2)
	{
		$html = '<span class="label label-warning">Tools/Consumables</span>';
	}else	if($po_for==3)
	{
		$html = '<span class="label label-danger">Instrument</span>';
	
	}
	 return $html;
}

function get_payment_status($val)
{
	if($val==0)
	{
		return '<span class="label label-danger">No</span>';
		
	}elseif($val==1)
	{
		return '<span class="label label-warning">Pending</span>';
	}
	elseif($val==5)
	{
		return '<span class="label label-success">Completed</span>';
	}
}

function get_po_status($val)
{
	if($val==1)
	{
		return '<span class="label label-info">New</span>';
		
	}elseif($val==2)
	{
		return '<span class="label label-warning">Pending</span>';
	}
	elseif($val==5)
	{
		return '<span class="label label-success">Completed</span>';
	}
}

function get_supplier_po_button($id,$po_status,$url,$rurl)
{
    $ci= & get_instance();
    $html='<span class="actions">';
	if($po_status==0)
	{
		$html .='<a href="'.  base_url().$rurl.'receive/'.$id.'" title="Receive Material"><i class="glyphicon glyphicon-th-list"></i></a> &nbsp;';
	    $html .='<a href="'.  base_url(). $url.'operation/'.$id.'" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a> &nbsp;';
	    $html .='<a href="'.  base_url(). $url.'delete/'.$id.'" onclick="conformboxdelete(this.href)" title="Delete"><i class="glyphicon glyphicon-trash" style="color:red;"></i></a> 	&nbsp;';
	}
	elseif($po_status>0 && $po_status<5)
	{
				$html .='<a href="'.  base_url().$rurl.'receive/'.$id.'" title="Receive Material"><i class="glyphicon glyphicon-th-list"></i></a> &nbsp;';

		$html .='<a href="'.  base_url().$url.'mailpo/'.$id.'" title="Send Mail"><i class="glyphicon glyphicon-envelope"></i></a> &nbsp;';
	}		
		$html .='<a href="'.  base_url(). $url.'view/'.$id.'" target="_blank" title="View PO Details"><i class="glyphicon glyphicon-eye-open"></i></a> &nbsp;';	
		$html .='<a href="'.  base_url(). $url.'printpo/'.$id.'" target="_blank" title="Print Po"><i class="glyphicon glyphicon-print"></i></a> &nbsp;';	
	
		$html.='</span>';	
	
    return $html;
}


function get_store_po_button($id,$po_status,$url,$rurl)
{
    $ci= & get_instance();
    $html='<span class="actions">';
	
	if($po_status==0)
	{
		if($ci->auth_level == 9)
		{
			$html .='<a href="'.base_url().$rurl.'receive/'.$id.'" title="Receive Material"><i class="glyphicon glyphicon-th-list"></i></a> &nbsp;';
			$html .='<a href="'.base_url().$url.'operation/'.$id.'" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a> &nbsp;';
			$html .='<a href="'.base_url().$url.'delete/'.$id.'" onclick="conformboxdelete(this.href)" title="Delete"><i class="glyphicon glyphicon-trash" style="color:red;"></i></a>&nbsp;';
		}else
		{
			$html .='<a href="'.base_url().$rurl.'receive/'.$id.'" title="Receive Material"><i class="glyphicon glyphicon-th-list"></i></a> &nbsp;';
		}
		
	}
		
		$html .='<a href="'.base_url().$url.'view/'.$id.'" target="_blank" title="View PO Details">
					<i class="glyphicon glyphicon-eye-open"></i>
				</a> &nbsp;';	

		$html.='</span>';	
	
    return $html;
}
//editing end by vinoth


function get_fil_status($status)
	{
		if($status == 0)
		{
			$html='<i class="fa fa-minus" style="color:red;"></i>';
		}
		elseif($status == 1)
		{
			$html='<i class="fa fa-check" style="color:green"></i>';
		}	
		
	    return $html;
	}
	function get_fil_details($id, $url, $po_status)
	{
	    $ci= & get_instance();
	    $html='<span class="actions">';
		$html .='&nbsp;<a href="'.  base_url(). $url.'receivepo/'.$id.'" title="View Details"><i class="glyphicon glyphicon-list"></i></a>';
		
		if($po_status==1)
		{
			$html .='<a href="'.  base_url(). $url.'edit/'.$id.'" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>';
			$html .='<a href=javascript:getConfirm("'.  base_url(). $url.'delete/'.$id.'"); title="Delete"><i class="glyphicon glyphicon-trash"></i></a>';
		}
		
		$html.='</span>';
	    return $html;
	}