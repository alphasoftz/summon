<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Statistics_model extends CI_Model
{
	const TABLE_NAME = 'settings';
	

	public function __construct()
	{
		parent::__construct();
	}

	public function getCountValue($table)
	{
		return $this->db->count_all($table);
	}

	public function paid($month,$year)
	{
		$sql="SELECT count(*) FROM `summons` WHERE MONTH(summon_date) = ".$month." AND YEAR(summon_date) = ".$year." AND `s_status`='2' GROUP BY MONTH(summon_date)";
		$query = $this->db->query($sql);
		return $query->row_array();
	}

	public function all($month,$year)
	{
		$sql="SELECT count(*) FROM `summons` WHERE MONTH(summon_date) = ".$month." AND YEAR(summon_date) = ".$year." GROUP BY MONTH(summon_date)";
		$query = $this->db->query($sql);
		return $query->row_array();
	}

	public function notpaid($month,$year)
	{
		$sql="SELECT count(*) FROM `summons` WHERE MONTH(summon_date) = ".$month." AND YEAR(summon_date) = ".$year." AND `s_status`='1' GROUP BY MONTH(summon_date)";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
}