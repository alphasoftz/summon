<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Summon_model extends CI_Model
{
	private $table = 'summons';
  
	function __construct() 
	{
	    parent::__construct();
	}

	public function insert_item($data)
	{
	    $this->db->insert('summon_types', $data);
		$result = $this->db->affected_rows();
		return $result;
	}
	public function insert_summon($data)
	{
		$this->db->insert('summons', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	function edit_item($data)
	{
		$this->db->update('summon_types' , $data , array('st_id' => $data['st_id']));
		$result = $this->db->affected_rows();
		return $result;	
	}
	public function remove_item($itemid)
	{
	   $this->db->delete('summon_types', array('st_id' => $itemid));
	}

	public function get_row_count()
	{
	    return $this->db->count_all($this->table);
	}

	public function record_count() {
        return $this->db->count_all("summon_types");
    }
    public function record_count_summons() {
        return $this->db->count_all("summons");
    }
    public function record_count_suggestions() {
        return $this->db->count_all("suggestions");
    }
    
	public function record_count_summons_paid() {
        $this->db->select('*');
        $this->db->from('summons');
        $this->db->where('s_status','2');
        $result = $this->db->affected_rows();
		return $result;	
    }  

    public function record_count_summons_pending() {
        $this->db->select('*');
        $this->db->from('summons');
        $this->db->where('s_status','1');
        $result = $this->db->affected_rows();
		return $result;	
    }    


    public function record_count_mysummons() {
        $user = $this->ion_auth->user()->row();
        $user_id=$user->id;
        $this->db->select('*');
        $this->db->from('summons');
        $this->db->where('user_id',$user_id);
        $result = $this->db->affected_rows();
        return $result; 
    }    

	public function fetch_types($limit, $start) {
        $this->db->limit($limit, $start);
        $this->db->from("summon_types AS a");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

   public function fetch_summons($limit, $start) {
        $this->db->limit($limit, $start);
        $this->db->from("summons AS a");
        $this->db->join("users as u","u.id=a.user_id");
        $this->db->join("summon_types as t","t.st_id=a.summon_type");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
   public function fetch_suggestions($limit, $start) {
        $this->db->limit($limit, $start);
        $this->db->from("suggestions AS a");
        $this->db->join("users as u","u.id=a.user_id");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
   
   public function fetch_mysummons($limit, $start) {
        $this->db->limit($limit, $start);
        
        $this->db->join("users as u","u.id=summons.user_id");
        $this->db->join("summon_types as t","t.st_id=summons.summon_type");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

   public function fetch_summons_paid($limit, $start) {
        
        
        $this->db->join("users as u","u.id=summons.user_id");
        $this->db->join("summon_types as t","t.st_id=summons.summon_type");
        
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

   public function fetch_summons_pending($limit, $start) {
        $this->db->limit($limit, $start);
        
        $this->db->join("users as u","u.id=summons.user_id");
        $this->db->join("summon_types as t","t.st_id=summons.summon_type");
        
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

   
  

   function get_data_id($id)
	{
		$query = $this->db->get_where('summon_types',array('st_id' => $id),1);
		return $query;
	}

	public function getUsers($value='')
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('username!=','administrator');
		$results = $this->db->get()->result();
		return $results;
	}
	public function getTypes($value='')
	{
		$this->db->select('*');
		$this->db->from('summon_types');
		$this->db->where('st_status','1');
		$results = $this->db->get()->result();
		return $results;
	}
    public function summonsonmap($value='')
    {
        $this->db->select('*');
        $this->db->from("summons AS a");
        $this->db->join("users as u","u.id=a.user_id");
        $this->db->join("summon_types as t","t.st_id=a.summon_type");
        $this->db->where('a.s_status','1');
        $results = $this->db->get()->result();
        return $results;
    }

    public function insert_suggestion($data)
    {
        $this->db->insert('suggestions', $data);
        $result = $this->db->affected_rows();
        return $result;
    }

    public function get_summon_details($sid)
    {
    	$this->db->select('*');
        $this->db->from("summons AS a");
        $this->db->join("users as u","u.id=a.user_id");
        $this->db->join("summon_types as t","t.st_id=a.summon_type");
        $this->db->where('a.sid',$sid);
        $results = $this->db->get()->result();
        return $results;
    }

    public function process_payment($sid)
    {
    	$data = array('sid' => $sid,'s_status'=>'2');
		$this->db->update('summons' , $data , array('sid' => $data['sid']));
		$result = $this->db->affected_rows();
		return $result;	
    }
    public function get_user_details($id)
    {
        $this->db->select('phone');
        $this->db->from("users");
        $this->db->join("summon_types as t","t.st_id=a.summon_type");
        $this->db->where('id',$id);
        $results = $this->db->get()->row_array();
        return $results['phone'];
    }

}