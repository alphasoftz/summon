<?php
/**
 *
 * Dynmic_menu.php
 *
 */
class Dynamic_menu {

    private $ci;                // for CodeIgniter Super Global Reference.

    private $id_menu        = 'id="nav-main"';
    private $class_menu        = 'class=""';
    private $class_parent    = 'class=""';
    private $class_last        = 'class=""';

    // --------------------------------------------------------------------

    /**
     * PHP5        Constructor
     *
     */
    function __construct()
    {
        $this->ci =& get_instance();    // get a reference to CodeIgniter.
    }

    // --------------------------------------------------------------------

    /**
     * build_menu($table, $type)
     *
     * Description:
     *
     * builds the Dynaminc dropdown menu
     * $table allows for passing in a MySQL table name for different menu tables.
     * $type is for the type of menu to display ie; topmenu, mainmenu, sidebar menu
     * or a footer menu.
     *
     * @param    string    the MySQL database table name.
     * @param    string    the type of menu to display.
     * @return    string    $html_out using CodeIgniter achor tags.
     */
    function build_menu($table = 'menu_items')
    {
        $menu = array();

        $where = array('menu_id' => '1','parent_id'=>'0');
        
        // use active record database to get the menu.
        $query = $this->ci->db->get_where($table,$where);

        if ($query->num_rows() > 0)
        {

            foreach ($query->result() as $row)
            {
                $menu[$row->mi_id]['mi_id']            = $row->mi_id;
                $menu[$row->mi_id]['menu_id']          = $row->menu_id;
                $menu[$row->mi_id]['display_name']     = $row->display_name;
                $menu[$row->mi_id]['page_id']          =  $row->page_id;
                $menu[$row->mi_id]['sort']             = $row->sort;
                $menu[$row->mi_id]['sort']             = $row->sort;
                $menu[$row->mi_id]['parent_id']        = $row->parent_id;
                $menu[$row->mi_id]['is_parent']        = $row->is_parent;
                $menu[$row->mi_id]['having_sub']        = $row->having_sub;
            }
        }
        
        $query->free_result();    // The $query result object will no longer be available

        // ----------------------------------------------------------------------     
        // now we will build the dynamic menus.
        // ----------------------------------------------------------------------
        $html_out="";
		$html_out .= "\t\t".'<ul '.$this->class_menu.' '. $this->id_menu .'>'."\n";
        $html_out .='<li><a href="'.base_url().'"><i class="fa fa-home"></i> Home </a></li>';       
        foreach ($menu as $key => $value) 
        {
                $catname=ucfirst($value['display_name']);
            
               
                    
                    if($value['having_sub']==1)
                    {
                        $html_out .= "\t\t\t".'<li><a href="javascript:void(0);">'.$catname.'</a>'; 
                        $html_out .= $this->get_childs($value['mi_id']);
                    }
                    else
                    {
                       $html_out .= "\t\t\t".'<li>'.anchor(str_replace(" ", "-", $catname), $catname ,'');
                    }
                    $html_out .= '</li>'."\n";
                
            
        }
       
        $html_out .= "\t\t".'</ul>' . "\n";
       
        return $html_out;
    }  
	
    /**
     * get_childs($menu, $parent_id) - SEE Above Method.
     *
     * Description:
     *
     * Builds all child submenus using a recurse method call.
     *
     * @param    mixed    $menu    array()
     * @param    string    $parent_id    id of parent calling this method.
     * @return    mixed    $html_out if has subcats else FALSE
     */

    function get_childs($parent_id)
    {

        $table='menu_items';
        $menu = array();

        $where = array('menu_id' => '1','parent_id'=>$parent_id);
        //$this->ci->db->where($where);
        // use active record database to get the menu.
        $query = $this->ci->db->get_where($table,$where);

        if ($query->num_rows() > 0)
        {

            foreach ($query->result() as $row)
            {
                $submenu[$row->mi_id]['mi_id']            = $row->mi_id;
                $submenu[$row->mi_id]['menu_id']          = $row->menu_id;
                $submenu[$row->mi_id]['display_name']     = $row->display_name;
                $submenu[$row->mi_id]['page_id']          =  $row->page_id;
                $submenu[$row->mi_id]['sort']             = $row->sort;
                $submenu[$row->mi_id]['sort']             = $row->sort;
                $submenu[$row->mi_id]['parent_id']        = $row->parent_id;
                $submenu[$row->mi_id]['is_parent']        = $row->is_parent;
            }
        }
        
        $query->free_result(); 
        
       
        $html_out="";
        
        $html_out .= "\t\t\t\t\t".'<ul id='.$parent_id.'>'."\n";
        
        foreach ($submenu as $key => $value) 
        {
             $catname=ucfirst($value['display_name']);

            $html_out .= "\t\t\t\t\t\t".'<li>'.anchor(str_replace(" ", "-", $catname), $catname ,'');
            $html_out .= '</li>' . "\n";
            
        }
        $html_out .= "\t\t\t\t\t".'</ul>' . "\n";
       

        return $html_out;
       
    }
    function clean_url($string)
    {
        $url=strtolower($string);
        $url=str_replace(array("'",'"'), '', $url);
        $url=str_replace(array(' ','+', '!', '&','-'), '-', $url);
        $url=str_replace("?", "", $url);
        $url=str_replace("---", "-", $url);
        $url=str_replace("--", "-", $url);
        return $url;
    }
}