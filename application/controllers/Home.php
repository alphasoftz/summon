<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
    {
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));
		//$this->load->model('en_add_model');
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->lang->load('auth');
		//$this->load->model('home_model');
		$this->load->model('m_slider');
 
    }
	public function index()
	{
		$view_data['table_data']='';	
		$view_data['footer_menu']=$this->en_menu_model->getMenuforSidebar('3');
		$view_data['slider']=$this->m_slider->getAll();
		$view_data['sb_news_data']		= $this->en_news_model->getAllForSidebar('5');
		$view_data['sb_events_data']	= $this->en_events_model->getAllForSidebar('5');
		$view_data['sb_video_data']		= $this->en_video_model->getAllForSidebar('5');
		$view_data['sb_gallery_data']	= $this->en_gallery_model->getAllForSidebar('5');
		$view_data['sb_gallery_data']	= $this->en_gallery_model->getAllForSidebar('5');
		$data = array(
				'title'       => $this->uri->segment('1')."Chellammal Backend",
				'content'     => $this->load->view('home',$view_data,TRUE),
				'content_add' => $this->load->view('home_add',$view_data,TRUE),                
				);
		$this->load->view('templates/home_template', $data); 
	}
	public function getPages()
	{
			//echo str_replace("_", " ", $this->uri->segment('1'));
		$page_name= str_replace("-", " ", $this->uri->segment('1'));
		$page_id=$this->home_model->getPageIDByName($page_name);
		//SIDEBAR SPECIAL MENUS DATA
		$view_data['sb_news_data']		= $this->en_news_model->getAllForSidebar('5');
		$view_data['sb_events_data']	= $this->en_events_model->getAllForSidebar('5');
		$view_data['sb_video_data']		= $this->en_video_model->getAllForSidebar('5');
		$view_data['sb_gallery_data']	= $this->en_gallery_model->getAllForSidebar('5');
		//SPECIAL MENU FOR FOOTER
		$view_data['footer_menu']=$this->en_menu_model->getMenuforSidebar('3');
				//IMAGE GALLERY MODULE
			if($page_id=='9001')
			{
				//PAGE DATA
				$view_data['sbitval']=array('2','3','4');
				$view_data['page_type']=$page_type;
				$view_data['albums']=$this->en_gallery_model->getAllForSidebar();

				$data = array(
		                    'title'       	=> 'Gallery',
		                    'meta_keywords'	=> META_KEYWORDS,
		                    'meta_desc'		=> META_DESC,
		                    'content'     	=> $this->load->view('home',$view_data,TRUE),
		                    );
		        $this->load->view('templates/gallery_template', $data); 
			}
			elseif($page_id=='9006')
			{
				//PAGE DATA
				$view_data['sbitval']=array('2','3','4');
				$view_data['page_type']=$page_type;
				$view_data['faq']=$this->en_faq_model->getAll();

				$data = array(
		                    'title'       	=> 'FAQ',
		                    'meta_keywords'	=> META_KEYWORDS,
		                    'meta_desc'		=> META_DESC,
		                    'content'     	=> $this->load->view('home',$view_data,TRUE),
		                    );
		        $this->load->view('templates/faq_template', $data); 
			}
			//VIDEO GALLERY MODULE
			elseif($page_id=='9002')
			{
				$view_data['sbitval']=array('2','3','4');
				$view_data['page_type']=$page_type;
				$view_data['videos']=$this->en_video_model->getAllForSidebar();

				$data = array(
				'title'       	=> 'Videos',
				'meta_keywords'	=> META_KEYWORDS,
				'meta_desc'		=> META_DESC,
				'content'     	=> $this->load->view('home',$view_data,TRUE),
				);
		        $this->load->view('templates/video_template', $data); 

			}//EVENTS  MODULE
			elseif($page_id=='9003')
			{
				$view_data['sb_events_data'] = $this->en_events_model->getAllForSidebar('10');
				$view_data['sbitval']=array('2','4');
				$view_data['page_type']=$page_type;
				$view_data['no_of_events'] = $this->en_events_model->noOfEvents();
						$data = array(
		                    'title'       	=> 'Events',
		                    'meta_keywords'	=> META_KEYWORDS,
		                    'meta_desc'		=> META_DESC,
		                    'content'     	=> $this->load->view('events',$view_data,TRUE),
		                    );
		        $this->load->view('templates/events_template', $data); 
			}
			//NEWS MODULE
			elseif($page_id=='9004')
			{
				$view_data['sb_news_data'] = $this->en_news_model->getAllForSidebar('10');
				$view_data['no_of_news'] = $this->en_news_model->noOfNews();
				$view_data['sbitval']=array('3','4');
				$view_data['page_type']=$page_type;
				$data = array(
		                    'title'       	=> 'News',
		                    'meta_keywords'	=> META_KEYWORDS,
		                    'meta_desc'		=> META_DESC,
		                    'content'     	=> $this->load->view('home',$view_data,TRUE),
		                    );
		        $this->load->view('templates/news_template', $data); 
			}
			elseif($page_id!='')
			{
				$pagedata=$this->home_model->getPageCredentials($page_id);
				foreach ($pagedata as $key => $value) 
				{
					$enpagesid=$value->enpagesid;
					$page_title=$value->page_title;
					$show_status=$value->show_status;
					$tags_keywords=$value->tags_keywords;
					$meta_desc=$value->tags_description;
					$page_short_content=$value->page_short_content;
					$page_content=$value->page_content;
					$page_images=$value->page_images;
					$page_videos=$value->page_videos;
					$created_date=$value->created_date;
					$last_updated=$value->last_updated;
					$last_updated_by=$value->last_updated_by;
					$sbit_val=$value->sbit_val;
					$sb_menus=$value->sb_menus;

				}
				//PAGE DATA
				$view_data['page_data']=$this->home_model->getPageCredentials($page_id);

				//PAGE IMAGES
				if($page_images!='')
				{
					$page_images=explode(',', $page_images);
					$imagearray=array();
					foreach ($page_images as $value) 
					{
						$imagearray[]=$this->home_model->getImageDetails($value);
					}
					$view_data['imagearray']=$imagearray;
				}

				//PAGE SIDEBAR MENUS
				if($sb_menus!='')
				{
					$sb_menus=explode(',', $sb_menus);
					$menuarray=array();
					foreach ($sb_menus as $value) 
					{
						$menuarray[]=$this->en_menu_model->getMenuforSidebar($value);
					}
					$view_data['menuarray']=$menuarray;
					
				}
				//SPECIAL ELEMENTS FOR SIDEBAR LIKE NEWS EVENTS GALLERY
				if($sbit_val!='')
				{
					$sbit_val=explode(',', $sbit_val);
					$sbitval=array();
					foreach ($sbit_val as $value) {
						
						$sbitval[]=$value;
					}
					$view_data['sbitval']=$sbitval;
				}
				$view_data['page_type']=$page_type;
				$data = array(
							'title'       	=> $page_title,
							'meta_keywords'	=> $tags_keywords,
							'meta_desc'		=> $page_short_content,
							'content'     	=> $this->load->view('home',$view_data,TRUE),
							);
				$this->load->view('templates/page_template', $data); 
			}
			else
			{
				$view_data['table_data']='';
				$view_data['page_type']=$page_type;	
				//$view_data['sbitval']=array('3,4,5');
				$data = array(
		                    'title'       => 'Page Not Found!',
		                    'content'     => $this->load->view('home',$view_data,TRUE),
		                    'content_add' => $this->load->view('home_add',$view_data,TRUE),                
		                    );
		        $this->load->view('templates/404_template', $data); 
			}			
	}
	
	function get_more_events($offset)
	{
		$data['sb_events_data'] = $this->en_events_model->get_events($offset);
		$this->load->view('get_events_pages', $data);
	}
	function get_more_news($offset)
	{
		$data['sb_news_data'] = $this->en_news_model->get_news($offset);
		$this->load->view('get_news_pages', $data);
	}
}
