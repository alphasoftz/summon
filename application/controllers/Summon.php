<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Summon extends CI_Controller {

	/*private $product = array(
		'soap' => array('name' => 'Brand X Soap', 'desc' => 'a bar of soap for showering', 'price' => '2.95', 'code' => 'sp001'), 
		'lotion' => array('name' => 'Skin Lotion', 'desc' => '100ml - Dry skins no more', 'price' => '4.50', 'code' => 'lt004'));
	*/
	private $product=array();
	private $currency = 'USD'; // currency for the transaction
	private $ec_action = 'Sale'; // for PAYMENTREQUEST_0_PAYMENTACTION, it's either Sale, Order or Authorization

	function __construct()
    {
		parent::__construct();
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->library('pagination');
		$this->load->helper("url");
        $this->load->model('summon_model');
		$paypal_details = array(
			// you can get this from your Paypal account, or from your
			// test accounts in Sandbox
			'API_username' => 'gtheleo_api1.gmail.com', 
			'API_signature' => 'A3QOUjpmro1-24yGmfY1zGIS6idMAWlNAeNMimXMvk5mip7HopvS4Zdy', 
			'API_password' => 'H9TJKY53S2PSC6JJ',
			// Paypal_ec defaults sandbox status to true
			// Change to false if you want to go live and
			// update the API credentials above
			// 'sandbox_status' => false,
		);
		$this->load->library('paypal_ec', $paypal_details);
	}
	public function create()
	{
		if ($this->ion_auth->logged_in())
		{
			if($this->ion_auth->is_admin())
			{
				

				if(isset($_POST['add']))
				{
					$this->form_validation->set_rules('summon_name', 'Description', 'required');
					$this->form_validation->set_rules('summon_type', 'Type', 'required');
					$this->form_validation->set_rules('user_id', 'User', 'required');

					if ($this->form_validation->run() == true)
					{
						
						if($_POST['sid']!='')
						{	
							$user = $this->ion_auth->user()->row();
							$data = array(
								'st_id'		  => $this->input->post('st_id'),
								'st_type'        => $this->input->post('st_type'),
								'st_desc'        => $this->input->post('st_desc'),
								'charges'=> $this->input->post('charges'),
								'st_status'      => '1'
							);
							
							//print_r($data);
						    $update = $this->summon_model->edit_item($data);
							if($update)
							{
								$this->session->set_flashdata('res', 'Summon Type updated Successfully');
								$this->session->set_flashdata('res_type', 'success');
								
								redirect(base_url().'master/types');
							}
							else
							{
								$this->session->set_flashdata('res', 'Something went wrong');
								$this->session->set_flashdata('res_type', 'danger');
								redirect(base_url().'master/types');
							}
							
						}
						else
						{
							$user = $this->ion_auth->user()->row();
							$data = array(
								'summon_name'        => $this->input->post('summon_name'),
								'summon_type'        => $this->input->post('summon_type'),
								'user_id'=> $this->input->post('user_id'),
								'summon_date' => date('Y-m-d H:i:s'),
								's_status'      => '1'
							);
							
							$inres = $this->summon_model->insert_summon($data);
							if($inres)
							{
								
								$summon_details=$this->summon_model->get_summon_details($inres);
								foreach ($summon_details as $key => $value) {
									$sid=$value->sid;
									$summon_name=$value->summon_name;
									$summon_type=$value->summon_type;
									$user_id=$value->user_id;
									$summon_date=$value->summon_date;
									$s_status=$value->s_status;
									$st_desc=$value->st_desc;
									$charges=$value->charges;
									$phone=$value->phone;
									$first_name=$value->first_name;
								}

								
									$this->send_sms($phone,'Hello '.$first_name.', You have been charged MYR '.$charges.' for '. $summon_name.'! Login and pay to avoid penalities!');
								

											
								$this->session->set_flashdata('res', 'Summon added Successfully');
								$this->session->set_flashdata('res_type', 'success');
								redirect(base_url().'summon/create');
							}
							else
							{
								$this->session->set_flashdata('res', 'Something went wrong');
								$this->session->set_flashdata('res_type', 'danger');
								redirect(base_url().'summon/create');
							}
							
						}
						
					}
				}
				
				$view_data['users']=$this->summon_model->getUsers();
				$view_data['types']=$this->summon_model->getTypes();
				$data = array(
		                    'title'     => "Summon Type Mangement",
		                    'content'   =>$this->load->view('summon/create',$view_data,TRUE)                
		                    );
		        $this->load->view('templates/template', $data); 
		    }
		    else
		    {
		    	$this->session->set_flashdata('message', 'You must be an administrator to view this page');
        		redirect('welcome/index');
		    }
		}
		else
		{
			redirect('auth/login', 'refresh');
		}
        
	}

	public function manage()
	{
		if ($this->ion_auth->logged_in())
		{
			if($this->ion_auth->is_admin())
			{
				$config = array();
		        $config["base_url"] = base_url() . "summon/manage";
		        $config["total_rows"] = $this->summon_model->record_count_summons();
		        $config["per_page"] = 10;
		        $config["uri_segment"] = 3;

		        $config['full_tag_open']    = '<li>';
	            $config['full_tag_close']   = '</li>';
	            $config['first_link']       = FALSE;
	            $config['first_tag_open']   = '<li>';
	            $config['first_tag_close']  = '</li>';
	            $config['last_link']        = FALSE;
	            $config['last_tag_open']    = '<li>';
	            $config['last_tag_close']   = '</li>';
	            $config['next_link']        = '&gt;';
	            $config['next_tag_open']    = '<li>';
	            $config['next_tag_close']   = '</li>';
	            $config['prev_link']        = '&lt;';
	            $config['prev_tag_open']    = '<li>';
	            $config['prev_tag_close']   = '</li>';
	            $config['cur_tag_open']     = '<li class="disabled"><a href="#">';
	            $config['cur_tag_close']    = '</a></li>';
	            $config['num_tag_open']     = '<li>';
	            $config['num_tag_close']    = '</li>';

		        $this->pagination->initialize($config);

		        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		        $view_data["results"] = $this->summon_model->fetch_summons($config["per_page"], $page);
		        $view_data["links"] = $this->pagination->create_links();
		        $data = array(
		                    'title'     => "Summons Mangement",
		                    'content'   =>$this->load->view('summon/manage',$view_data,TRUE)                
		                    );
		        $this->load->view('templates/template', $data); 
			}
			else
		    {
		    	$this->session->set_flashdata('message', 'You must be an administrator to view this page');
        		redirect('welcome/index');
		    }
		}
		else
		{
			redirect('auth/login', 'refresh');
		}
	}

	public function send_sms($to,$message)
    {
        $this->load->library('plivo');

        $sms_data = array(
            'src' => '+919047705568', //The phone number to use as the caller id (with the country code). E.g. For USA 15671234567
            'dst' => $to, // The number to which the message needs to be send (regular phone numbers must be prefixed with country code but without the ‘+’ sign) E.g., For USA 15677654321.
            'text' => $message, // The text to send
            'type' => 'sms', //The type of message. Should be 'sms' for a text message. Defaults to 'sms'
            'url' => base_url() . 'plivo_test/receive_sms', // The URL which will be called with the status of the message.
            'method' => 'POST', // The method used to call the URL. Defaults to. POST
        );

        /*
         * look up available number groups
         */
        $response_array = $this->plivo->send_sms($sms_data);

        if ($response_array[0] == '200')
        {
            $data = json_decode($response_array[1], TRUE);

            //print_r($data["response"]);
        }
        else
        {
            /*
             * the response wasn't good, show the error
             */
            $data=$this->api_error($response_array);
        }
        return $data;
    }

	function paynow() 
	{
		$sid=$this->uri->segment('3');
		$summon_details=$this->summon_model->get_summon_details($sid);
		foreach ($summon_details as $key => $value) {
			$sid=$value->sid;
			$summon_name=$value->summon_name;
			$summon_type=$value->summon_type;
			$user_id=$value->user_id;
			$summon_date=$value->summon_date;
			$s_status=$value->s_status;
			$st_desc=$value->st_desc;
			$charges=$value->charges;
		}

		$to_buy = array(
			'desc' => $summon_name, 
			'currency' => $this->currency, 
			'type' => $this->ec_action, 
			'return_URL' => site_url('summon/check_payment_status'), 
			// see below have a function for this -- function back()
			// whatever you use, make sure the URL is live and can process
			// the next steps
			'cancel_URL' => site_url('summon/manage'), // this goes to this controllers index()
			'shipping_amount' => 0.00, 
			'get_shipping' => false);
		// I am just iterating through $this->product from defined
		// above. In a live case, you could be iterating through
		// the content of your shopping cart.
		//foreach($this->product as $p) {
			$temp_product = array(
				'name' => $summon_name, 
				'desc' => $st_desc, 
				'number' => $sid, 
				'quantity' => 1, // simple example -- fixed to 1
				'amount' => $charges);
				
			// add product to main $to_buy array
			$to_buy['products'][] = $temp_product;
		//}
		// enquire Paypal API for token
		$set_ec_return = $this->paypal_ec->set_ec($to_buy);
		if (isset($set_ec_return['ec_status']) && ($set_ec_return['ec_status'] === true)) {
			// redirect to Paypal
			$this->paypal_ec->redirect_to_paypal($set_ec_return['TOKEN']);
			// You could detect your visitor's browser and redirect to Paypal's mobile checkout
			// if they are on a mobile device. Just add a true as the last parameter. It defaults
			// to false
			// $this->paypal_ec->redirect_to_paypal( $set_ec_return['TOKEN'], true);
		} else {
			$this->_error($set_ec_return);
		}
	}

	function check_payment_status() {
		// we are back from Paypal. We need to do GetExpressCheckoutDetails
		// and DoExpressCheckoutPayment to complete.
		$token = $_GET['token'];
		$payer_id = $_GET['PayerID'];
		// GetExpressCheckoutDetails
		$get_ec_return = $this->paypal_ec->get_ec($token);
		if (isset($get_ec_return['ec_status']) && ($get_ec_return['ec_status'] === true)) {
			// at this point, you have all of the data for the transaction.
			// you may want to save the data for future action. what's left to
			// do is to collect the money -- you do that by call DoExpressCheckoutPayment
			// via $this->paypal_ec->do_ec();
			//
			// I suggest to save all of the details of the transaction. You get all that
			// in $get_ec_return array
			$ec_details = array(
				'token' => $token, 
				'payer_id' => $payer_id, 
				'currency' => $this->currency, 
				'amount' => $get_ec_return['PAYMENTREQUEST_0_AMT'], 
				'IPN_URL' => site_url('test/ipn'), 
				// in case you want to log the IPN, and you
				// may have to in case of Pending transaction
				'type' => $this->ec_action);
				
			// DoExpressCheckoutPayment
			$do_ec_return = $this->paypal_ec->do_ec($ec_details);
			if (isset($do_ec_return['ec_status']) && ($do_ec_return['ec_status'] === true)) {
				// at this point, you have collected payment from your customer
				// you may want to process the order now.

				$this->session->set_flashdata('message', 'Thank you. We will process your order now.! Your Summon is clear! Drive Safe and follow road rules!!');
				//echo "<pre>";
				//echo "\nGetExpressCheckoutDetails Data\n" . print_r($get_ec_return, true);
				$paid_sid=$get_ec_return['L_PAYMENTREQUEST_0_NUMBER0'];

				$summon_details=$this->summon_model->get_summon_details($paid_sid);
				foreach ($summon_details as $key => $value) {
					$sid=$value->sid;
					$summon_name=$value->summon_name;
					$summon_type=$value->summon_type;
					$user_id=$value->user_id;
					$summon_date=$value->summon_date;
					$s_status=$value->s_status;
					$st_desc=$value->st_desc;
					$charges=$value->charges;
					$phone=$value->phone;
				}

				$pay_success=$this->summon_model->process_payment($paid_sid);
				if($pay_success=='1')
				{
					$this->send_sms($phone,'Thank you! We received your payment! Please follow road rules :)');
				}

				//echo "\n\nDoExpressCheckoutPayment Data\n" . print_r($do_ec_return, true);
				//echo "</pre>";
				redirect('summon/manage');
			} 
			else 
			{
				$this->_error($do_ec_return);
			}
		} 
		else 
		{
			$this->_error($get_ec_return);
		}
	}

	public function paid()
	{
		if ($this->ion_auth->logged_in())
		{
			if($this->ion_auth->is_admin())
			{
				$config = array();
		        $config["base_url"] = base_url() . "summon/paid";
		        $config["total_rows"] = $this->summon_model->record_count_summons_paid();
		        $config["per_page"] = 3;
		        $config["uri_segment"] = 3;

		        $config['full_tag_open']    = '<li>';
	            $config['full_tag_close']   = '</li>';
	            $config['first_link']       = FALSE;
	            $config['first_tag_open']   = '<li>';
	            $config['first_tag_close']  = '</li>';
	            $config['last_link']        = FALSE;
	            $config['last_tag_open']    = '<li>';
	            $config['last_tag_close']   = '</li>';
	            $config['next_link']        = '&gt;';
	            $config['next_tag_open']    = '<li>';
	            $config['next_tag_close']   = '</li>';
	            $config['prev_link']        = '&lt;';
	            $config['prev_tag_open']    = '<li>';
	            $config['prev_tag_close']   = '</li>';
	            $config['cur_tag_open']     = '<li class="disabled"><a href="#">';
	            $config['cur_tag_close']    = '</a></li>';
	            $config['num_tag_open']     = '<li>';
	            $config['num_tag_close']    = '</li>';

		        $this->pagination->initialize($config);

		        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		        $view_data["results"] = $this->summon_model->fetch_summons_paid($config["per_page"], $page);
		        $view_data["links"] = $this->pagination->create_links();
		        $data = array(
		                    'title'     => "Summons Mangement",
		                    'content'   =>$this->load->view('summon/paid',$view_data,TRUE)                
		                    );
		        $this->load->view('templates/template', $data); 
			}
			else
		    {
		    	$this->session->set_flashdata('message', 'You must be an administrator to view this page');
        		redirect('welcome/index');
		    }
		}
		else
		{
			redirect('auth/login', 'refresh');
		}
	}

	public function pending()
	{
		if ($this->ion_auth->logged_in())
		{
			if($this->ion_auth->is_admin())
			{
				$config = array();
		        $config["base_url"] = base_url() . "summon/pending";
		        $config["total_rows"] = $this->summon_model->record_count_summons_pending();
		        $config["per_page"] = 3;
		        $config["uri_segment"] = 3;

		        $config['full_tag_open']    = '<li>';
	            $config['full_tag_close']   = '</li>';
	            $config['first_link']       = FALSE;
	            $config['first_tag_open']   = '<li>';
	            $config['first_tag_close']  = '</li>';
	            $config['last_link']        = FALSE;
	            $config['last_tag_open']    = '<li>';
	            $config['last_tag_close']   = '</li>';
	            $config['next_link']        = '&gt;';
	            $config['next_tag_open']    = '<li>';
	            $config['next_tag_close']   = '</li>';
	            $config['prev_link']        = '&lt;';
	            $config['prev_tag_open']    = '<li>';
	            $config['prev_tag_close']   = '</li>';
	            $config['cur_tag_open']     = '<li class="disabled"><a href="#">';
	            $config['cur_tag_close']    = '</a></li>';
	            $config['num_tag_open']     = '<li>';
	            $config['num_tag_close']    = '</li>';

		        $this->pagination->initialize($config);

		        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		        $view_data["results"] = $this->summon_model->fetch_summons_pending($config["per_page"], $page);
		        $view_data["links"] = $this->pagination->create_links();
		        $data = array(
		                    'title'     => "Summons Mangement",
		                    'content'   =>$this->load->view('summon/pending',$view_data,TRUE)                
		                    );
		        $this->load->view('templates/template', $data); 
			}
			else
		    {
		    	$this->session->set_flashdata('message', 'You must be an administrator to view this page');
        		redirect('welcome/index');
		    }
		}
		else
		{
			redirect('auth/login', 'refresh');
		}
	}
	public function edit($id)
	{		
		if ($this->ion_auth->logged_in())
		{
			$config = array();
	        $config["base_url"] = base_url() . "master/types";
	        $config["total_rows"] = $this->summon_model->record_count();
	        $config["per_page"] = 3;
	        $config["uri_segment"] = 3;

	        $config['full_tag_open']    = '<li>';
            $config['full_tag_close']   = '</li>';
            $config['first_link']       = FALSE;
            $config['first_tag_open']   = '<li>';
            $config['first_tag_close']  = '</li>';
            $config['last_link']        = FALSE;
            $config['last_tag_open']    = '<li>';
            $config['last_tag_close']   = '</li>';
            $config['next_link']        = '&gt;';
            $config['next_tag_open']    = '<li>';
            $config['next_tag_close']   = '</li>';
            $config['prev_link']        = '&lt;';
            $config['prev_tag_open']    = '<li>';
            $config['prev_tag_close']   = '</li>';
            $config['cur_tag_open']     = '<li class="disabled"><a href="#">';
            $config['cur_tag_close']    = '</a></li>';
            $config['num_tag_open']     = '<li>';
            $config['num_tag_close']    = '</li>';

	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
	        $view_data["results"] = $this->summon_model->fetch_types($config["per_page"], $page);
	        $view_data["links"] = $this->pagination->create_links();

			if($id)
			{
				$view_data['value']=$this->summon_model->get_data_id($id);
				$data = array(
		                    'title'     => "Summon Type Management - Edit",
		                    'content'   =>$this->load->view('master/index',$view_data,TRUE)                
		                    );
		        $this->load->view('templates/template', $data); 
				
			}

		}
		else
		{
			redirect('auth/login', 'refresh');
		}
	}
	public function delete($id)
	{
		if ($this->ion_auth->logged_in())
		{
			$delete=$this->summon_model->remove_item($id);
			$this->session->set_flashdata('res', "Summon type and it's associated data deleted successfully");
			$this->session->set_flashdata('res_type', 'success');
			redirect(base_url().'master/types');
		}
		else
		{
			redirect('auth/login', 'refresh');
		}
		
	}
	public function mysummons()
	{
		if ($this->ion_auth->logged_in())
		{
			$config = array();
	        $config["base_url"] = base_url() . "summon/mysummons";
	        $config["total_rows"] = $this->summon_model->record_count_mysummons();
	        $config["per_page"] = 3;
	        $config["uri_segment"] = 3;

	        $config['full_tag_open']    = '<li>';
            $config['full_tag_close']   = '</li>';
            $config['first_link']       = FALSE;
            $config['first_tag_open']   = '<li>';
            $config['first_tag_close']  = '</li>';
            $config['last_link']        = FALSE;
            $config['last_tag_open']    = '<li>';
            $config['last_tag_close']   = '</li>';
            $config['next_link']        = '&gt;';
            $config['next_tag_open']    = '<li>';
            $config['next_tag_close']   = '</li>';
            $config['prev_link']        = '&lt;';
            $config['prev_tag_open']    = '<li>';
            $config['prev_tag_close']   = '</li>';
            $config['cur_tag_open']     = '<li class="disabled"><a href="#">';
            $config['cur_tag_close']    = '</a></li>';
            $config['num_tag_open']     = '<li>';
            $config['num_tag_close']    = '</li>';

	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $view_data["results"] = $this->summon_model->fetch_mysummons($config["per_page"], $page);
	        $view_data["links"] = $this->pagination->create_links();
	        $data = array(
	                    'title'     => "My Summons",
	                    'content'   =>$this->load->view('summon/mysummons',$view_data,TRUE)                
	                    );
	        $this->load->view('templates/template', $data); 
			
		}
		else
		{
			redirect('auth/login', 'refresh');
		}
	}


	/* -------------------------------------------------------------------------------------------------
	* a simple message to display errors. this should only be used during development
	* --------------------------------------------------------------------------------------------------
	*/
	function _error($ecd) {
		echo "<br>error at Express Checkout<br>";
		echo "<pre>" . print_r($ecd, true) . "</pre>";
		echo "<br>CURL error message<br>";
		echo 'Message:' . $this->session->userdata('curl_error_msg') . '<br>';
		echo 'Number:' . $this->session->userdata('curl_error_no') . '<br>';
	}

	/**
     * A function to show an error response
     * @param type $response
     */
    public function api_error($response)
    {
        echo "<h2>Error</h2>\n";

        print_r($response);
    }

}