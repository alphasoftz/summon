<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
    {
		parent::__construct();
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		
	}
	public function index()
	{
		if ($this->ion_auth->logged_in())
		{
			
			$data = array(
	                    'title'     => "Welcome",
	                    'content'   =>$this->load->view('common/restricted',$view_data,TRUE)                
	                    );
	        $this->load->view('templates/template', $data); 
		   
		}
		else
		{
			redirect('auth/login', 'refresh');
		}
        
	}
}