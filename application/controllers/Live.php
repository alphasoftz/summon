<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Live extends CI_Controller {

	function __construct()
    {
		parent::__construct();
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));		
		$this->load->library('upload');
		$this->load->library("pagination");
		$this->load->library('datatables');
        $this->load->library('table');
        $this->load->model('summon_model');
        $this->load->library('googlemaps');
	}
	public function index()
	{
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		$config['center'] = '2.626141, 113.512186';
		$config['zoom'] = '5';
		$this->googlemaps->initialize($config);
		$live_trace=$this->summon_model->summonsonmap();

		foreach ($live_trace as $key => $value) 
		{
			$marker = array();
			$marker['position'] = $value->lat.','.$value->lon;
			$marker['infowindow_content'] = $value->first_name.' '.$value->last_name;
			$marker['animation'] = 'DROP';
			$marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=*|f44336|ffffff';
			$this->googlemaps->add_marker($marker);	
		}

		$view_data['map'] = $this->googlemaps->create_map();

		$data = array(
                    'title'     => "Live Trace",
                    'content'   =>$this->load->view('live/index',$view_data,TRUE)                
                    );
        $this->load->view('templates/template', $data); 
        
	}
}