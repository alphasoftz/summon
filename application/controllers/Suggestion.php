<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suggestion extends CI_Controller {

	function __construct()
    {
		parent::__construct();
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->library('pagination');
		$this->load->helper("url");
        $this->load->model('summon_model');
		
	}
	public function index()
	{
		if ($this->ion_auth->logged_in())
		{
				
				if(isset($_POST['add']))
				{
					$this->form_validation->set_rules('suggestion', 'Suggestion', 'required');
					

					if ($this->form_validation->run() == true)
					{
						
							$user = $this->ion_auth->user()->row();
							$data = array(
								'suggestion'        => $this->input->post('suggestion'),
								'user_id'=> $user->id,
								'sg_date' => date('Y-m-d H:i:s'),
								'sg_status'      => '1'
							);
							
							$inres = $this->summon_model->insert_suggestion($data);
							if($inres)
							{
								$this->session->set_flashdata('res', 'Suggestion Sent Successfully');
								$this->session->set_flashdata('res_type', 'success');
								redirect(base_url().'suggestion/index');
							}
							else
							{
								$this->session->set_flashdata('res', 'Something went wrong');
								$this->session->set_flashdata('res_type', 'danger');
								redirect(base_url().'suggestion/index');
							}
							
						
						
					}
				}
				
				$view_data['users']=$this->summon_model->getUsers();
				$view_data['types']=$this->summon_model->getTypes();
				$data = array(
		                    'title'     => "Summon - Suggestions",
		                    'content'   =>$this->load->view('suggestions/index',$view_data,TRUE)                
		                    );
		        $this->load->view('templates/template', $data); 
		}
		else
		{
			redirect('auth/login', 'refresh');
		}
        
	}

	

	

	
	public function edit($id)
	{		
		if ($this->ion_auth->logged_in())
		{
			$config = array();
	        $config["base_url"] = base_url() . "master/types";
	        $config["total_rows"] = $this->summon_model->record_count();
	        $config["per_page"] = 3;
	        $config["uri_segment"] = 3;

	        $config['full_tag_open']    = '<li>';
            $config['full_tag_close']   = '</li>';
            $config['first_link']       = FALSE;
            $config['first_tag_open']   = '<li>';
            $config['first_tag_close']  = '</li>';
            $config['last_link']        = FALSE;
            $config['last_tag_open']    = '<li>';
            $config['last_tag_close']   = '</li>';
            $config['next_link']        = '&gt;';
            $config['next_tag_open']    = '<li>';
            $config['next_tag_close']   = '</li>';
            $config['prev_link']        = '&lt;';
            $config['prev_tag_open']    = '<li>';
            $config['prev_tag_close']   = '</li>';
            $config['cur_tag_open']     = '<li class="disabled"><a href="#">';
            $config['cur_tag_close']    = '</a></li>';
            $config['num_tag_open']     = '<li>';
            $config['num_tag_close']    = '</li>';

	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
	        $view_data["results"] = $this->summon_model->fetch_types($config["per_page"], $page);
	        $view_data["links"] = $this->pagination->create_links();

			if($id)
			{
				$view_data['value']=$this->summon_model->get_data_id($id);
				$data = array(
		                    'title'     => "Summon Type Management - Edit",
		                    'content'   =>$this->load->view('master/index',$view_data,TRUE)                
		                    );
		        $this->load->view('templates/template', $data); 
				
			}

		}
		else
		{
			redirect('auth/login', 'refresh');
		}
	}
	public function delete($id)
	{
		if ($this->ion_auth->logged_in())
		{
			$delete=$this->summon_model->remove_item($id);
			$this->session->set_flashdata('res', "Summon type and it's associated data deleted successfully");
			$this->session->set_flashdata('res_type', 'success');
			redirect(base_url().'master/types');
		}
		else
		{
			redirect('auth/login', 'refresh');
		}
		
	}

	public function manage()
	{
		if ($this->ion_auth->logged_in())
		{
			if($this->ion_auth->is_admin())
			{
				$config = array();
		        $config["base_url"] = base_url() . "suggestion/manage";
		        $config["total_rows"] = $this->summon_model->record_count_suggestions();
		        $config["per_page"] = 3;
		        $config["uri_segment"] = 3;

		        $config['full_tag_open']    = '<li>';
	            $config['full_tag_close']   = '</li>';
	            $config['first_link']       = FALSE;
	            $config['first_tag_open']   = '<li>';
	            $config['first_tag_close']  = '</li>';
	            $config['last_link']        = FALSE;
	            $config['last_tag_open']    = '<li>';
	            $config['last_tag_close']   = '</li>';
	            $config['next_link']        = '&gt;';
	            $config['next_tag_open']    = '<li>';
	            $config['next_tag_close']   = '</li>';
	            $config['prev_link']        = '&lt;';
	            $config['prev_tag_open']    = '<li>';
	            $config['prev_tag_close']   = '</li>';
	            $config['cur_tag_open']     = '<li class="disabled"><a href="#">';
	            $config['cur_tag_close']    = '</a></li>';
	            $config['num_tag_open']     = '<li>';
	            $config['num_tag_close']    = '</li>';

		        $this->pagination->initialize($config);

		        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		        $view_data["results"] = $this->summon_model->fetch_suggestions($config["per_page"], $page);
		        $view_data["links"] = $this->pagination->create_links();
		        $data = array(
		                    'title'     => "Suggestions Mangement",
		                    'content'   =>$this->load->view('suggestions/manage',$view_data,TRUE)                
		                    );
		        $this->load->view('templates/template', $data); 
			}
			else
		    {
		    	$this->session->set_flashdata('message', 'You must be an administrator to view this page');
        		redirect('welcome/index');
		    }
		}
		else
		{
			redirect('auth/login', 'refresh');
		}
	}
}